import { CardContainer } from '@/components/CoinComponents';
import { ProgressTable } from '@/components/ProgressTable';
import { Meta } from '@/layout/Meta';
import { Main } from '@/templates/Main';
import { ProgressItem } from '@/utils/constants';

const Progress = () => {
  const progressItems: ProgressItem[] = [
    {
      createdBy: 'hocky',
      coinToken: 'BNB',
      percentage: 30.69,
      status: 'in_progress',
      timePeriod: 300,
    },
    {
      createdBy: 'hocky',
      coinToken: 'USDT',
      percentage: 100,
      status: 'done',
      timePeriod: 300,
    },
    {
      createdBy: 'hocky',
      coinToken: 'CAKE',
      percentage: 70.23,
      status: 'cancelled',
      timePeriod: 300,
    },
  ];
  return (
    <Main
      meta={<Meta title="Admin page" description="Check tracking progress" />}
      title={'Admin'}
    >
      <CardContainer>
        <ProgressTable progressItems={progressItems} isAdmin={true} />
      </CardContainer>
    </Main>
  );
};

export default Progress;
