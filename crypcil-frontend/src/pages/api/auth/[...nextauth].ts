import axios from 'axios';
import { createSecretKey } from 'crypto';
import { jwtVerify } from 'jose';
import NextAuth from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';

import { LOGIN_URL } from '@/utils/constants';

export default NextAuth({
  providers: [
    CredentialsProvider({
      name: 'crypcil',
      credentials: {
        username: {
          label: 'Username',
          type: 'text',
          placeholder: 'Username',
        },
        password: {
          label: 'Password',
          type: 'password',
          placeholder: 'Password',
        },
      },
      async authorize(credentials, req) {
        // Add logic here to look up the user from the credentials supplied
        const creds = {
          username: credentials.username,
          password: credentials.password,
          grant_type: 'password',
          client_id: process.env.CLIENT_ID,
          client_secret: process.env.CLIENT_SECRET,
        };
        const res = await axios.post(LOGIN_URL, creds, {
          headers: {
            accept: '*/*',
            'Content-Type': 'application/json',
          },
        });
        try {
          const access_token = await res.data.access_token;
          if (res.status === 200 && access_token) {
            const key = createSecretKey(
              process.env.CRYPCIL_AUTH_SECRET,
              'utf-8'
            );
            const result = await jwtVerify(access_token, key);
            const data = result.payload;
            // console.log(data);
            return data;
          }
        } catch (e) {
          console.error(e);
          return null;
        }
      },
    }),
  ],
  callbacks: {
    async jwt(jwtObject) {
      // console.log('JWT');
      // console.log(jwtObject);
      if (jwtObject.user) {
        const { iat, exp, ...rest } = jwtObject.user;
        jwtObject.token = { ...rest, ...jwtObject.token };
      }
      return jwtObject.token;
    },
    async session(sessionObject) {
      const { iat, exp, jti, ...rest } = sessionObject.token;
      sessionObject.session.user = { ...rest, ...sessionObject.session.user };
      return sessionObject.session;
    },
  },
  secret: process.env.CRYPCIL_AUTH_SECRET,
  pages: {
    signIn: '/login',
    error: '/login',
  },
  theme: {
    colorScheme: 'light',
    brandColor: '#0ea5e9',
  },
});
