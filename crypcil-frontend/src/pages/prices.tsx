import axios from 'axios';

import { CardContainer, CoinCard } from '@/components/CoinComponents';
import { Meta } from '@/layout/Meta';
import { Main } from '@/templates/Main';
import { TOKEN_SELECTIONS } from '@/utils/crypto';

const Prices = ({ data }) => (
  <Main
    meta={<Meta title="Prices" description="See all coins prices" />}
    title="Prices"
  >
    <CardContainer>
      {TOKEN_SELECTIONS.map(function (object, i) {
        return (
          <CoinCard
            key={i}
            coins={data.data[object.id]}
            token={TOKEN_SELECTIONS[i]}
          />
        );
      })}
    </CardContainer>
  </Main>
);

// This gets called on every request
export async function getServerSideProps() {
  // Fetch data from external API
  const { data } = await axios.get(
    'https://track-crypcil.hocky.id/track/harga'
  );
  // console.log(data)
  // // Pass data to the page via props
  // console.log('{ props: { data } } = ', { props: { data } });
  return { props: { data } };
}

export default Prices;
