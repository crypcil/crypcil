import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons';
import {
  Box,
  Button,
  CircularProgress,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  InputRightElement,
  useToast,
} from '@chakra-ui/react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { useState } from 'react';

import { Meta } from '@/layout/Meta';
import { Main } from '@/templates/Main';
import { REGISTER_URL } from '@/utils/constants';

const Index = () => {
  const router = useRouter();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [address, setAddress] = useState('');

  const [isLoading, setIsLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  const toast = useToast();

  async function userRegister(param: {
    password: string;
    address: string;
    username: string;
  }) {
    try {
      if (!/^0x[a-fA-F0-9]{40}$/g.test(address)) {
        throw { message: 'Address should match BNB regex' };
      }
      const res = await axios.post(REGISTER_URL, param);
      return res;
    } catch (e) {
      console.error(e);
      if (e.response?.data) throw e.response?.data;
      throw e;
    }
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    setIsLoading(true);

    try {
      await userRegister({ username, password, address });
      toast({
        title: 'Register suceeded',
        description: "You're in",
        status: 'success',
        duration: 9000,
        isClosable: true,
      });
      setIsLoading(false);
      setShowPassword(false);
    } catch (error) {
      toast({
        title: 'Failed to register!',
        description: error.message,
        status: 'error',
        duration: 9000,
        isClosable: true,
      });
      setIsLoading(false);
    }
  };

  const handlePasswordVisibility = () => setShowPassword(!showPassword);
  return (
    <Main
      meta={<Meta title="Register Page" description="Crypcil Register Page" />}
      title={'Register'}
    >
      <Box
        className="bg-white"
        mx={4}
        p={8}
        width="100%"
        borderWidth={1}
        borderRadius={8}
        boxShadow="lg"
      >
        <Box my={4} textAlign="left">
          <form onSubmit={handleSubmit} className="flex flex-col space-y-4">
            <FormControl isRequired>
              <FormLabel>BNB Address</FormLabel>
              <Input
                placeholder="BNB Address"
                size="lg"
                onChange={(event) => setAddress(event.currentTarget.value)}
              />
            </FormControl>
            <FormControl isRequired>
              <FormLabel>Username</FormLabel>
              <Input
                placeholder="Username"
                size="lg"
                onChange={(event) => setUsername(event.currentTarget.value)}
              />
            </FormControl>
            <FormControl isRequired>
              <FormLabel>Password</FormLabel>
              <InputGroup>
                <Input
                  type={showPassword ? 'text' : 'password'}
                  placeholder="Password"
                  size="lg"
                  onChange={(event) => setPassword(event.currentTarget.value)}
                />
                <InputRightElement width="4.5rem">
                  <Button
                    onClick={handlePasswordVisibility}
                    h="1.75rem"
                    size="sm"
                  >
                    {showPassword ? <ViewIcon /> : <ViewOffIcon />}
                  </Button>
                </InputRightElement>
              </InputGroup>
            </FormControl>
            <div className="flex flex-row space-x-5">
              <Button variant="outline" type="submit" width="full" mt={4}>
                {isLoading ? (
                  <CircularProgress isIndeterminate size="24px" color="blue" />
                ) : (
                  'Register'
                )}
              </Button>
              <Button
                colorScheme="blue"
                onClick={() => router.push('/login')}
                width="full"
                mt={4}
              >
                {isLoading ? (
                  <CircularProgress isIndeterminate size="24px" color="blue" />
                ) : (
                  'Sign In'
                )}
              </Button>
            </div>
          </form>
        </Box>
      </Box>
    </Main>
  );
};

export default Index;
