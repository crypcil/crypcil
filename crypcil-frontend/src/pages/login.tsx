import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons';
import {
  Box,
  Button,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  InputRightElement,
  useToast,
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { getCsrfToken, getSession } from 'next-auth/react';
import { useEffect, useState } from 'react';

import { Meta } from '@/layout/Meta';
import { Main } from '@/templates/Main';

const Index = ({ csrfToken }) => {
  const router = useRouter();
  const errorLogin = router.query.error;
  const [showPassword, setShowPassword] = useState(false);
  // async function userLogin(param: {
  //   password: string;
  //   username: string;
  //   csrfToken: string;
  // }) {
  //   try {
  //     const res = await axios.post('/api/auth/callback/credentials', param);
  //     return res;
  //   } catch (e) {
  //     console.error(e);
  //     if (e.response?.data) throw e.response?.data;
  //     throw e;
  //   }
  // }

  const toast = useToast();
  useEffect(() => {
    if (errorLogin) {
      toast({
        title: errorLogin.includes('401')
          ? 'Wrong credentials! 😱'
          : 'Auth server has problem! 🙏',
        description: errorLogin,
        status: 'error',
        duration: 9000,
        isClosable: true,
      });
    }
  }, []);

  const handlePasswordVisibility = () => setShowPassword(!showPassword);
  return (
    <Main
      meta={<Meta title="Login Page" description="Crypcil Login Page" />}
      title={'Login'}
    >
      <Box
        className="bg-white"
        mx={4}
        p={8}
        width="100%"
        borderWidth={1}
        borderRadius={8}
        boxShadow="lg"
      >
        <Box my={4} textAlign="left">
          <form
            method="post"
            // onSubmit={handleSubmit}
            action="/api/auth/callback/credentials"
            className="flex flex-col space-y-4"
          >
            <input name="csrfToken" type="hidden" defaultValue={csrfToken} />

            <FormControl isRequired>
              <FormLabel>Username</FormLabel>
              <Input name="username" placeholder="test@test.com" size="lg" />
            </FormControl>
            <FormControl isRequired>
              <FormLabel>Password</FormLabel>
              <InputGroup>
                <Input
                  name="password"
                  type={showPassword ? 'text' : 'password'}
                  placeholder="Password"
                  size="lg"
                />
                <InputRightElement width="4.5rem">
                  <Button
                    onClick={handlePasswordVisibility}
                    h="1.75rem"
                    size="sm"
                  >
                    {showPassword ? <ViewIcon /> : <ViewOffIcon />}
                  </Button>
                </InputRightElement>
              </InputGroup>
            </FormControl>
            <div className="flex flex-row space-x-5">
              <Button
                variant="outline"
                onClick={() => router.push('/register')}
                width="full"
                mt={4}
              >
                Register
              </Button>
              <Button colorScheme="blue" type="submit" width="full" mt={4}>
                Sign In
              </Button>
            </div>
          </form>
        </Box>
      </Box>
    </Main>
  );
};

export default Index;

export async function getServerSideProps(context) {
  const { req } = context;
  const session = await getSession({ req });
  if (session) {
    return {
      redirect: { destination: '/' },
    };
  }

  return {
    props: {
      csrfToken: await getCsrfToken(context),
    },
  };
}
