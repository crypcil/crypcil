import axios from 'axios';
import { getSession } from 'next-auth/react';

import { CardContainer, CoinInvestmentCard } from '@/components/CoinComponents';
import { Meta } from '@/layout/Meta';
import { Main } from '@/templates/Main';
import { TOKEN_SELECTIONS_OBJ_ID } from '@/utils/crypto';

const Portfolio = ({ data, currentPrice }) => {
  const { allCoinsAmount } = data.data;
  const totalInvestments = 400;
  const profit = 3.73;
  const profitPercentage = 20;
  return (
    <Main
      meta={<Meta title="Portfolio" description="See investments" />}
      title="Portfolio"
    >
      <div className="flex w-full flex-col items-center space-y-10">
        {/*<div className="text-center">*/}
        {/*  <div className="font-bold">Total Investments: {totalInvestments}</div>*/}
        {/*  <div className="font-bold">Profit: ${profit} USD</div>*/}
        {/*  <div className="font-bold">*/}
        {/*    Profit Percentage: {profitPercentage}%*/}
        {/*  </div>*/}
        {/*</div>*/}
        <CardContainer>
          {Object.keys(allCoinsAmount).map((value, index) => {
            return (
              <CoinInvestmentCard
                key={index}
                token={TOKEN_SELECTIONS_OBJ_ID[value]}
                amount={allCoinsAmount[value]}
                price={currentPrice[value]}
              />
            );
          })}
        </CardContainer>
      </div>
    </Main>
  );
};

// This gets called on every request
export async function getServerSideProps(context) {
  const session = await getSession(context);
  const { address } = session.user;
  // Fetch data from external API
  const { data } = await axios.get(
    `https://returns-crypcil.hocky.id/wallet/${address}/amount`
  );
  const currentPrice = (
    await axios.get('https://track-crypcil.hocky.id/track/harga')
  ).data.data;
  // // Pass data to the page via props
  // console.log('{ props: { data } } = ', { props: { data } });
  return { props: { data, currentPrice } };
}

export default Portfolio;
