import { Image } from '@chakra-ui/react';
import { useSession } from 'next-auth/react';

import { CardContainer } from '@/components/CoinComponents';
import { Meta } from '@/layout/Meta';
import { Main } from '@/templates/Main';

const Index = () => {
  const { data: session } = useSession();
  let title = 'About';
  if (session) {
    const userData = session!.user;
    // @ts-ignore
    title = `Welcome, ${userData?.username ?? 'Anonymous'}`;
  }
  return (
    <Main
      meta={
        <Meta title="About page" description="Everything about the website" />
      }
      title={title}
    >
      <div className="flex flex-col items-center space-y-10">
        <CardContainer>
          <div>
            This project was made to fulfill the final project of CSCE604271 -
            Webservices. Check out the{' '}
            <a href="https://gitlab.com/crypcil/crypcil#tim-pengembang-developers">
              repo
            </a>{' '}
            for more details. TL;DR, a cryptocurrency transaction tracking app.
          </div>
          <Image
            src="https://gitlab.com/crypcil/crypcil/-/raw/main/README.assets/architecture.png"
            alt="architecture"
          />
          <div className="mt-10 text-xs">
            <a href="https://github.com/hockyy">hocky 🍁</a> was here
          </div>
        </CardContainer>
      </div>
    </Main>
  );
};

export default Index;
