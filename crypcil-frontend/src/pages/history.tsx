import axios from 'axios';
import { getSession } from 'next-auth/react';

import {
  CardContainer,
  TransactionHistoryCard,
} from '@/components/CoinComponents';
import { Meta } from '@/layout/Meta';
import { Main } from '@/templates/Main';

const History = ({ data, currentPrice }) => {
  // const tmpData = {
  //   id: 112,
  //   hash: '0x5ead072e6fea8dbe7118410e216102a459b0d6377b1a261fd5a951a12dd11166',
  //   from: '0x7d71a9255e1a4ea452e6015c3bf64ccde6431d03',
  //   to: '0xf8ff065a820d107423d605f0eeaf4e33604ca4c7',
  //   wallet: '0x7d71a9255E1A4Ea452E6015C3Bf64ccDe6431D03',
  //   time: '2022-04-26T02:13:41.000Z',
  //   type: 'out',
  //   amount: 2,
  //   worth: 808.54,
  //   coinPrice: {
  //     time: '2022-04-26T02:14:00.345Z',
  //     coin: 'binancecoin',
  //     price: 404.27,
  //     market_cap: 67965799934.0183,
  //     total_volume: 2027247274.8820546,
  //   },
  //   note: null,
  // };
  return (
    <Main
      meta={<Meta title="Lorem ipsum" description="Lorem ipsum" />}
      title="Transaction History"
    >
      <div className="flex w-full flex-col items-center space-y-10">
        {/* <div className="text-center"> */}
        {/*  <div className="font-bold">Total Investments: {totalInvestments}</div> */}
        {/*  <div className="font-bold">Profit: ${profit} USD</div> */}
        {/*  <div className="font-bold">Profit Percentage: {profitPercentage}%</div> */}
        {/* </div> */}

        <CardContainer>
          {data.data.map((value, index) => {
            return (
              <TransactionHistoryCard
                key={index}
                trxHistoryItem={value}
                price={currentPrice[value.coin_id].usd}
              />
            );
          })}
        </CardContainer>
      </div>
    </Main>
  );
};

// This gets called on every request
export async function getServerSideProps(context) {
  const session = await getSession(context);
  const { address } = session.user;
  // Fetch data from external API
  const { data } = await axios.get(
    `https://returns-crypcil.hocky.id/wallet/${address}/history`
  );
  const currentPrice = (
    await axios.get('https://track-crypcil.hocky.id/track/harga')
  ).data.data;
  // // Pass data to the page via props
  // console.log('{ props: { data } } = ', { props: { data } });
  return { props: { data, currentPrice } };
}

export default History;
