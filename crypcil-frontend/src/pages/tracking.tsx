import {
  Box,
  Button,
  CircularProgress,
  FormControl,
  FormLabel,
  Select,
  Slider,
  SliderFilledTrack,
  SliderThumb,
  SliderTrack,
  useToast,
} from '@chakra-ui/react';
import { useState } from 'react';

import { Meta } from '@/layout/Meta';
import { Main } from '@/templates/Main';
import { fancyTimeFormat } from '@/utils/constants';
import { TOKEN_SELECTIONS } from '@/utils/crypto';

const percentToTime = (percent: number) => {
  return percent * 3 + 300;
};

const Tracking = () => {
  const initialPercent = 20;
  const [coin, setCoin] = useState('');
  const [timePeriod, setTimeperiod] = useState(percentToTime(initialPercent));
  const [isLoading, setIsLoading] = useState(false);
  const toast = useToast();

  async function sendTrackingQueue(param: {
    timePeriod: number;
    coin: string;
  }) {
    console.log(param);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    setIsLoading(true);

    try {
      await sendTrackingQueue({
        timePeriod,
        coin,
      });
      toast({
        title: 'Sended tracking request',
        description: `You're in ${coin}, ${timePeriod}`,
        status: 'success',
        duration: 9000,
        isClosable: true,
      });
      setIsLoading(false);
    } catch (error) {
      toast({
        title: 'Failed to send requests',
        description: error.message,
        status: 'error',
        duration: 9000,
        isClosable: true,
      });
      setIsLoading(false);
    }
  };

  return (
    <Main
      meta={
        <Meta
          title="Tracking page"
          description="Sending tracking request live"
        />
      }
      title={'Tracking'}
    >
      <form onSubmit={handleSubmit} className="flex w-6/12 flex-col space-y-4">
        <FormControl isRequired>
          <FormLabel>Coin</FormLabel>
          <Select
            onChange={(event) => setCoin(event.currentTarget.value)}
            placeholder="Select which coin to track"
            size="lg"
            bg="bg-white"
          >
            {TOKEN_SELECTIONS.map((val, idx) => {
              return (
                <option key={val.token} value={val.token}>
                  {val.name} ({val.token})
                </option>
              );
            })}
          </Select>
        </FormControl>
        <FormControl isRequired>
          <FormLabel>Time Period</FormLabel>
          <Slider
            aria-label="slider-ex-4"
            defaultValue={initialPercent}
            onChange={(event) => setTimeperiod(percentToTime(event))}
          >
            <SliderTrack bg="blue.100">
              <SliderFilledTrack bg="blue.400" />
            </SliderTrack>
            <SliderThumb boxSize={6}>
              <Box color="blue" />
            </SliderThumb>
          </Slider>
        </FormControl>
        <div className="text-center font-bold">
          {fancyTimeFormat(timePeriod)}
        </div>
        <Button colorScheme="blue" type="submit" width="full" mt={4}>
          {isLoading ? (
            <CircularProgress isIndeterminate size="24px" color="blue.400" />
          ) : (
            'Submit'
          )}
        </Button>
      </form>
    </Main>
  );
};

export default Tracking;
