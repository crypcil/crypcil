import {
  Button,
  Progress as ChakraProgress,
  Table,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from '@chakra-ui/react';

import {
  colorProgress,
  DONE,
  fancyTimeFormat,
  IN_PROGRESS,
  ProgressItem,
} from '@/utils/constants';

export const ProgressTable = ({
  progressItems,
  isAdmin,
}: {
  progressItems: ProgressItem[];
  isAdmin: boolean;
}) => {
  return (
    <TableContainer className="w-full">
      <Table variant="simple" className="w-full bg-white">
        <Thead>
          <Tr>
            {isAdmin ? <Th>By</Th> : <></>}
            <Th>Request</Th>
            <Th width="50%">Status</Th>
            <Th>Percentage</Th>
            <Th isNumeric>Action</Th>
          </Tr>
        </Thead>
        <Tbody>
          {progressItems.map((val, idx) => {
            return (
              <Tr key={idx}>
                {isAdmin ? <Td>{val.createdBy}</Td> : <></>}
                <Td>
                  {val.coinToken} / {fancyTimeFormat(val.timePeriod)}
                </Td>
                <Td>
                  <ChakraProgress
                    colorScheme={colorProgress[val.status]}
                    height="32px"
                    value={val.percentage}
                  />
                </Td>
                <Td className="">
                  <div className="flex flex-col items-end">
                    {val.percentage.toFixed(2)}%
                  </div>
                </Td>
                <Td>
                  <div className="flex flex-col space-y-2">
                    {val.status === DONE ? (
                      <>
                        <Button colorScheme="blue" size="xs">
                          Get Graph
                        </Button>
                        <Button colorScheme="blue" size="xs">
                          Get JSON
                        </Button>
                      </>
                    ) : val.status === IN_PROGRESS ? (
                      <Button colorScheme="red" size="xs">
                        Cancel
                      </Button>
                    ) : (
                      <></>
                    )}
                  </div>
                </Td>
              </Tr>
            );
          })}
        </Tbody>
      </Table>
    </TableContainer>
  );
};
