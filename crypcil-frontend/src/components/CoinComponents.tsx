import { AttachmentIcon } from '@chakra-ui/icons';
import { Box, Image } from '@chakra-ui/react';
import { ReactNode } from 'react';

import {
  CryptoFormat,
  CurrencyFormat,
  DAYS_OF_WEEK,
  MONTH_NAMES,
  NTH,
} from '@/utils/constants';
import {
  TOKEN_SELECTIONS_OBJ_ID,
  TokenInfo,
  TokenSelectionInfo,
} from '@/utils/crypto';

export const CoinCard = (props) => {
  // console.log(props)
  return (
    <Box className="mx-4 my-2 w-full rounded-lg border bg-gray-50 px-4 py-3">
      <div className="m-0 flex flex-row items-center justify-between ">
        <div className="m-4 flex flex-row items-center justify-center space-x-3">
          <Image src={props.token.image.src} boxSize="50px" alt="coin-logo" />
          <div className="m-0 flex flex-col space-y-2">
            <div className="m-0 text-gray-500">{props.token.name}</div>
            <div className="text-lg font-bold">{props.token.token}</div>
          </div>
        </div>
        <div className="m-2 flex flex-col items-end space-y-2">
          <div className="font-bold">${props.coins.usd}</div>
          <div>{props.coins.usd_24h_change.toFixed(2)}%</div>
        </div>
      </div>
    </Box>
  );
};

export const CoinInvestmentCard = ({
  token,
  amount,
  price,
}: {
  token: TokenSelectionInfo;
  amount: number;
  price: number;
}) => {
  console.log(price);
  console.log(amount);
  return (
    <Box className="mx-4 my-2 w-full rounded-lg border bg-gray-50 px-4 py-3">
      <div className="m-0 flex flex-row items-center justify-between ">
        <div className="m-4 flex flex-row items-center justify-center space-x-3">
          <Image src={token.image.src} boxSize="50px" alt="coin-logo" />
          <div className="m-0 flex flex-col space-y-1">
            <div className="text-lg font-bold">{token.token}</div>
            <div className="m-0 text-gray-500">{token.name}</div>
          </div>
        </div>
        <div className="m-2 flex flex-col items-end space-y-1">
          <div className="font-bold">
            {amount} {token.token}
          </div>
          <div>Worth ${price.usd * amount} USD</div>
        </div>
      </div>
    </Box>
  );
};

export type TransactionHistoryItem = {
  time: string;
  hash: string;
  from: string;
  to: string;
  amount: number;
  worth: number;
  type: string;
  coin_id: string;
};

export const timestampToReadableTime = (timestamp: string) => {
  const date = new Date(timestamp);
  const month = MONTH_NAMES[date.getMonth()];
  const day = date.getDate();
  const year = date.getFullYear();
  const dayOfWeek = DAYS_OF_WEEK[date.getDay()];
  const hours = String(date.getHours()).padStart(2, '0');
  const minutes = String(date.getMinutes()).padStart(2, '0');
  const seconds = String(date.getSeconds()).padStart(2, '0');
  return `${hours}:${minutes}:${seconds}\n${dayOfWeek ?? ''}, ${
    month ?? ''
  } ${day}${NTH(day)} ${year}`;
};

export const TransactionHistoryCard = ({
  trxHistoryItem,
  price,
}: {
  trxHistoryItem: TransactionHistoryItem;
  price: number;
}) => {
  const coin = TOKEN_SELECTIONS_OBJ_ID[trxHistoryItem.coin_id] as TokenInfo;
  const currentWorth = price * trxHistoryItem.amount;
  const pastWorth = trxHistoryItem.worth;
  const isIn = trxHistoryItem.type === 'in';
  const profit = isIn ? currentWorth - pastWorth : pastWorth - currentWorth;
  const profitPercent = profit / pastWorth;
  return (
    <Box className="mx-4 my-2 w-full rounded-lg border bg-gray-50 px-4 py-3">
      <div className="flex flex-col items-center space-y-2">
        <div className="flex text-center text-base font-bold">
          {timestampToReadableTime(trxHistoryItem.time)}
        </div>
        <div className="flex break-all text-center text-base font-bold text-blue-500">
          {trxHistoryItem.type === 'in'
            ? `From: ${trxHistoryItem.from}`
            : `To: ${trxHistoryItem.to}`}
        </div>
        <div className="flex w-11/12 flex-row items-center justify-between">
          <div className="flex flex-col items-center">
            <div className="text-lg font-bold">
              {trxHistoryItem.type.toUpperCase()}
            </div>
            <Image src={coin.image.src} boxSize="50px" alt="logo" />
            <div className="text-lg font-bold">{coin.token}</div>
          </div>
          <div className="flex flex-col items-end">
            <div className="text-lg font-bold">
              {CryptoFormat(trxHistoryItem.amount)} 🪙
            </div>
            <div>{`Current Worth ${CurrencyFormat(currentWorth)}`}</div>
            <div>{`Past Worth ${CurrencyFormat(pastWorth)}`}</div>
            <div>{`Profit/Loss ${profit < 0 ? '-' : ''}${CurrencyFormat(
              Math.abs(profit)
            )}`}</div>
            <div className="font-bold">{`${profitPercent.toFixed(2)}%`}</div>
          </div>
        </div>

        <div className="flex flex-col items-center rounded-lg border bg-blue-50 p-2 text-center">
          <div className="flex items-center justify-center space-x-2">
            <AttachmentIcon />
            <div className="text-lg font-bold">Transaction Hash</div>
          </div>
          <div className="break-all text-center">{trxHistoryItem.hash}</div>
        </div>
      </div>
    </Box>
  );
};

export const CardContainer = ({ children }: { children: ReactNode }) => {
  return (
    <div className="flex w-full flex-col items-center p-2">{children}</div>
  );
};
