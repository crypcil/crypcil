import { Image } from '@chakra-ui/react';
import { useSession } from 'next-auth/react';
import React, { useState } from 'react';

interface LinkNavbarData {
  href: string;
  text: string;
}

const url = ({ href, text }: LinkNavbarData) => {
  return (
    <a
      href={href}
      className="mt-4 mr-4 block text-lg text-blue-200 hover:text-white lg:mt-0 lg:inline-block"
    >
      {text}
    </a>
  );
};

const urlButton = ({ href, text }: LinkNavbarData) => {
  return (
    <a
      href={href}
      className="mt-4 inline-block rounded border border-white px-4 py-2 text-sm leading-none text-white hover:border-transparent hover:bg-white hover:text-blue-500 lg:mt-0"
    >
      {text}
    </a>
  );
};

const Navigation = () => {
  const [isExpanded, toggleExpansion] = useState(false);
  const { data: session } = useSession();
  return (
    <nav className="sticky top-0 flex flex-wrap items-center justify-between bg-sky-500 px-6 py-4 font-bold">
      <a
        href="/"
        className="mr-6 flex shrink-0 items-center text-3xl text-white"
      >
        <Image
          src="https://gitlab.com/crypcil/crypcil/-/raw/main/crypcil.png"
          alt="crypcil-logo"
          boxSize="50px"
        />
        <h2>Crypcil</h2>
      </a>
      <div className="block lg:hidden">
        <button
          className="flex items-center rounded border border-blue-400 px-3 py-2 text-sky-200 hover:border-white hover:text-white"
          onClick={() => toggleExpansion(!isExpanded)}
        >
          <svg
            className="h-3 w-3 fill-current"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Menu</title>
            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
          </svg>
        </button>
      </div>
      <div
        className={`${
          isExpanded ? `block` : `hidden`
        } w-full block flex-grow lg:flex lg:items-center lg:w-auto`}
      >
        <div className="text-sm lg:grow">
          {url({ href: '/portfolio/', text: 'Portfolio' })}
          {url({ href: '/prices/', text: 'Prices' })}
          {url({ href: '/history/', text: 'History' })}
          {url({ href: '/tracking/', text: 'Tracking' })}
          {url({ href: '/progress/', text: 'Progress' })}
          {url({ href: '/admin/', text: 'Admin' })}
        </div>
        {!session ? (
          <div>{urlButton({ href: '/login/', text: 'Login' })}</div>
        ) : (
          <div>{urlButton({ href: '/api/auth/signout/', text: 'Logout' })}</div>
        )}
      </div>
    </nav>
  );
};

export default Navigation;
