import { ReactNode } from 'react';

import Navigation from '@/layout/Navigation';
import { AppConfig } from '@/utils/AppConfig';

interface IMainProps {
  meta: ReactNode;
  children: ReactNode;
  title: string;
}

const Main = (props: IMainProps) => (
  <div className="w-full text-gray-700 antialiased">
    {props.meta}
    <Navigation />

    <div className="mx-auto max-w-screen-md">
      <div className="pt-16 pb-8">
        <div className="flex justify-center text-center text-5xl font-bold text-sky-500">
          {props.title}
        </div>
        {/* <div className="text-xl">{AppConfig.description}</div> */}
      </div>
      <div className="content flex justify-center py-5 text-xl">
        {props.children}
      </div>

      <div className="border-t border-gray-300 py-8 text-center text-sm">
        © Copyright {new Date().getFullYear()} {AppConfig.title}. Powered with{' '}
        <span role="img" aria-label="Love">
          ♥
        </span>{' '}
        by <a href="https://creativedesignsguru.com">CreativeDesignsGuru</a> and{' '}
        <a href="https://gitlab.com/crypcil/crypcil#tim-pengembang-developers">
          Crypcil Developers
        </a>
        {/*
         * PLEASE READ THIS SECTION
         * We'll really appreciate if you could have a link to our website
         * The link doesn't need to appear on every pages, one link on one page is enough.
         * Thank you for your support it'll mean a lot for us.
         */}
      </div>
    </div>
  </div>
);

export { Main };
