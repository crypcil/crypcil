export const CRYPCIL_AUTH_URL = process.env.NEXT_PUBLIC_CRYPCIL_AUTH_URL!;
export const REGISTER_URL = `${CRYPCIL_AUTH_URL}/register`;
export const LOGIN_URL = `${CRYPCIL_AUTH_URL}/login`;

export const MONTH_NAMES = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];
export const DAYS_OF_WEEK = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];
export const NTH = (d: number) => {
  if (d > 3 && d < 21) return 'th';
  switch (d % 10) {
    case 1:
      return 'st';
    case 2:
      return 'nd';
    case 3:
      return 'rd';
    default:
      return 'th';
  }
};

export const CurrencyFormat = (num: number) => {
  return `${num < 0 ? '-' : ''}$${Math.abs(num).toFixed(4)}`;
};

export const CryptoFormat = (num: number) => {
  return `${num.toFixed(6)}`;
};

export function fancyTimeFormat(duration: number) {
  // Hours, minutes and seconds
  const hrs = Math.floor(duration / 3600);
  const mins = Math.floor((duration % 3600) / 60);
  const secs = Math.floor(duration) % 60;

  // Output like "1:01" or "4:03:59" or "123:03:59"
  let ret = '';

  if (hrs > 0) {
    ret += `${hrs}:${mins < 10 ? '0' : ''}`;
  }

  ret += `${mins}'${secs < 10 ? '0' : ''}`;
  ret += `${secs}"`;
  return ret;
}

export type ProgressItem = {
  createdBy: string;
  coinToken: string;
  timePeriod: number;
  percentage: number;
  status: string;
};

export const DONE = 'done';
export const IN_PROGRESS = 'in_progress';
export const CANCELLED = 'cancelled';

export const colorProgress: Record<string, string> = {
  done: 'green',
  in_progress: 'yellow',
  cancelled: 'red',
};
