const BASE_URL = 'https://s2.coinmarketcap.com/static/img/coins/200x200/';
export type TokenInfo = {
  id: string;
  name: string;
  token: string;
  address: string;
  decimals: number;
  image: {
    src: string;
  };
};

export type TokenSelectionInfo = {
  id: string;
  name: string;
  token: string;
  image: {
    src: string;
  };
};

export const MAINNET_TOKENS: Record<string, TokenInfo> = {
  USDT: {
    id: 'tether',
    name: 'Tether USD',
    token: 'USDT',
    address: '0x55d398326f99059fF775485246999027B3197955',
    decimals: 18,
    image: {
      src: `${BASE_URL}825.png`,
    },
  },
  ALPACA: {
    id: 'alpaca-finance',
    name: 'Alpaca Finance',
    token: 'ALPACA',
    address: '0x8f0528ce5ef7b51152a59745befdd91d97091d2f',
    decimals: 18,
    image: {
      src: `${BASE_URL}8707.png`,
    },
  },
  BELT: {
    id: 'belt',
    name: 'Belt Finance',
    token: 'BELT',
    address: '0xe0e514c71282b6f4e823703a39374cf58dc3ea4f',
    decimals: 18,
    image: {
      src: `${BASE_URL}8730.png`,
    },
  },
  BUNNY: {
    id: 'pancake-bunny',
    name: 'Pancake Bunny',
    token: 'BUNNY',
    address: '0xc9849e6fdb743d08faee3e34dd2d1bc69ea11a51',
    decimals: 18,
    image: {
      src: `${BASE_URL}7791.png`,
    },
  },
  CAKE: {
    id: 'pancakeswap-token',
    name: 'PancakeSwap',
    token: 'CAKE',
    address: '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82',
    decimals: 18,
    image: {
      src: `${BASE_URL}7186.png`,
    },
  },
  BUSD: {
    id: 'binance-usd',
    name: 'Binance USD',
    token: 'BUSD',
    address: '0xe9e7cea3dedca5984780bafc599bd69add087d56',
    decimals: 18,
    image: {
      src: `${BASE_URL}4687.png`,
    },
  },
  TKO: {
    id: 'tokocrypto',
    name: 'Tokocrypto',
    token: 'TKO',
    address: '0x9f589e3eabe42ebc94a44727b3f3531c0c877809',
    decimals: 18,
    image: {
      src: `${BASE_URL}9020.png`,
    },
  },
  GALA: {
    id: 'gala',
    name: 'Gala',
    token: 'GALA',
    address: '0x7ddee176f665cd201f93eede625770e2fd911990',
    decimals: 18,
    image: {
      src: `${BASE_URL}7080.png`,
    },
  },
  BAKE: {
    id: 'bakerytoken',
    name: 'BakerySwap',
    token: 'BAKE',
    address: '0xe02df9e3e622debdd69fb838bb799e3f168902c5',
    decimals: 18,
    image: {
      src: `${BASE_URL}7064.png`,
    },
  },
  XVS: {
    id: 'venus',
    name: 'Venus',
    token: 'XVS',
    address: '0xcf6bb5389c92bdda8a3747ddb454cb7a64626c63',
    decimals: 18,
    image: {
      src: `${BASE_URL}7288.png`,
    },
  },
  BSW: {
    id: 'biswap',
    name: 'Biswap',
    token: 'BSW',
    address: '0x965f527d9159dce6288a2219db51fc6eef120dd1',
    decimals: 18,
    image: {
      src: `${BASE_URL}10746.png`,
    },
  },
  MBOX: {
    id: 'mobox',
    name: 'Mobox',
    token: 'MBOX',
    address: '0x3203c9e46ca618c8c1ce5dc67e7e9d75f5da2377',
    decimals: 18,
    image: {
      src: `${BASE_URL}9175.png`,
    },
  },
  BABY: {
    id: 'babyswap',
    name: 'BabySwap',
    token: 'BABY',
    address: '0x53e562b9b7e5e94b81f10e96ee70ad06df3d2657',
    decimals: 18,
    image: {
      src: `${BASE_URL}10334.png`,
    },
  },
  PACOCA: {
    id: 'pacoca',
    name: 'Pacoca',
    token: 'PACOCA',
    address: '0x55671114d774ee99d653d6c12460c780a67f1d18',
    decimals: 18,
    image: {
      src: `${BASE_URL}10522.png`,
    },
  },
};

export const TESTNET_TOKENS: Record<string, TokenInfo> = {
  USDT: {
    id: 'tether',
    name: 'Tether USD',
    token: 'USDT',
    address: '0x337610d27c682E347C9cD60BD4b3b107C9d34dDd',
    decimals: 18,
    image: {
      src: `${BASE_URL}825.png`,
    },
  },
  WBNB: {
    id: 'wbnb',
    name: 'Wrapped BNB',
    token: 'WBNB',
    address: '0xae13d989dac2f0debff460ac112a837c89baa7cd',
    decimals: 18,
    image: {
      src: `${BASE_URL}7192.png`,
    },
  },
  BUSD: {
    id: 'binance-usd',
    name: 'Binance USD',
    token: 'BUSD',
    address: '0x78867BbEeF44f2326bF8DDd1941a4439382EF2A7',
    decimals: 18,
    image: {
      src: `${BASE_URL}4687.png`,
    },
  },
  DAI: {
    id: 'dai',
    name: 'Dai',
    token: 'DAI',
    address: '0x8a9424745056Eb399FD19a0EC26A14316684e274',
    decimals: 18,
    image: {
      src: `${BASE_URL}4943.png`,
    },
  },
  ETH: {
    id: 'ethereum',
    name: 'Ethereum',
    token: 'ETH',
    address: '0xd66c6B4F0be8CE5b39D52E0Fd1344c389929B378',
    decimals: 18,
    image: {
      src: `${BASE_URL}1027.png`,
    },
  },
  SAFEMOON: {
    id: 'safemoon-2',
    name: 'Safemoon',
    token: 'SAFEMOON',
    address: '0xDAcbdeCc2992a63390d108e8507B98c7E2B5584a',
    decimals: 18,
    image: {
      src: `${BASE_URL}8757.png`,
    },
  },
};

export const TOKENS = MAINNET_TOKENS;

export const TOKEN_SELECTIONS: TokenSelectionInfo[] = [
  {
    id: 'binancecoin',
    name: 'Binance Coin',
    token: 'BNB',
    image: { src: `${BASE_URL}1839.png` },
  }, // add default BNB
  ...Object.values(TOKENS).map(({ id, name, token, image }) => ({
    id,
    name,
    token,
    image,
  })),
];

export const TOKEN_SELECTIONS_OBJ: Record<string, TokenSelectionInfo> = {};
TOKEN_SELECTIONS.forEach((element) => {
  TOKEN_SELECTIONS_OBJ[element.token] = element;
});

export const TOKEN_SELECTIONS_OBJ_ID: Record<string, TokenSelectionInfo> = {};
TOKEN_SELECTIONS.forEach((element) => {
  TOKEN_SELECTIONS_OBJ_ID[element.id] = element;
});

export const RPC_URL = 'https://data-seed-prebsc-1-s1.binance.org:8545/';

export const BSC_COIN_ID_LIST = TOKEN_SELECTIONS.filter(
  (token) => token.id
).map(({ id }) => id);

export const BSC_COIN_ID_CSV = BSC_COIN_ID_LIST.join(',');
