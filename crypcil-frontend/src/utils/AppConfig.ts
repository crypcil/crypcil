export const AppConfig = {
  site_name: 'Crypcil',
  title: 'Crypcil',
  description: 'Cryptocurrency Transaction Tracker',
  locale: 'en',
};
