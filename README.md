# crypcil

This project was made to fulfill the final project of CSCE604271 - Webservices.

Dibuat untuk memenuhi indikator evaluasi proyek akhir mata kuliah CSCE604271 - Layanan & Aplikasi Web.

![img](README.assets/architecture.png)

## App Requirements

- Memiliki mekanisme **login & logout** (aspek authentication & session)
- Memiliki suatu endpoint (dapat melakukan suatu tugas tertentu) yang di *restrict* untuk setiap user atau minimal setelah user login baru dapat mengakses layanan.
- Menerapkan OAuth untuk setiap service yang terlibat di dalamnya (aspek security)Memiliki dokumentasi yang benar & rapih. Misalkan: untuk akses suatu endpoint dibutuhkan param apa saja, contoh response-nya, skenario kemungkinannya seperti apa saja, jika error dibedakan melalui status codes dan message yang jelas. (aspek service contract, SOA, microservices, CRUD)
- **Memiliki task yang membutuhkan waktu yang agak lama (minimal 5 menit).**
- Menerapkan message queue (menggunakan message broker) misalnya untuk notify user utk mengambil data hasil prosesnya (aspek asynchronous)
- **Memiliki beberapa** **worker background** **yang dapat bekerja secara paralel** 
- Memiliki beberapa service yang di-deploy pada VM/container terpisah (aspek deployment)
- **Memiliki minimal 1 service yang merupakan choreography / orchestration atas beberapa service di belakangnya (aspek service orchestration)**
- Menerapkan load balancing untuk service-service yang memiliki potensi dipergunakan lebih banyak dari service yang lain
- **Menerapkan cache untuk layanan-layanan yang memiliki sifat** **idempoten**. Cache minimal 5 menit.
- Melakukan pengujian implementasi dengan skenario load, stress, dan endurance testing 
- Menerapkan gRPC untuk salah satu service
- Memiliki frontend yang cukup sederhana:
  - Dapat menjadi pintu masuk untuk memberikan argumen ke dalam sistem
  - Dapat menampilkan daftar proses yang sedang berjalan “di belakang”
  - Dapat menunjukkan status progress proses secara *real time*
  - Dapat menampilkan/mendownload hasil proses
- Memiliki log sentral untuk melihat seluruh log dari microservices yang ada 

## Tim Pengembang (Developers)

- Jovi Handono Hutama
- Hocky Yudhiono
- Eko Julianto Salim
- Faris Sayidinarechan Ardhafa
- Lazuardi Putra Pratama Nusantara
