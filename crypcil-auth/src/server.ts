import Fastify, { FastifyRequest, FastifyReply } from "fastify";
import fjwt, { JWT } from "fastify-jwt";
import swagger from "fastify-swagger";
import { withRefResolver } from "fastify-zod";
import { version } from "../package.json";
import crypcilRoutes from "./modules/crypcil/crypcil.route";
import {userSchemas} from "./modules/crypcil/crypcil.schema";
import 'dotenv/config' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
import fastifyCors from "@fastify/cors";

declare module "fastify" {
  interface FastifyRequest {
    jwt: JWT;
  }
  export interface FastifyInstance {
    authenticate: any;
  }
}

function buildServer() {
  const server = Fastify();

  server.register(fjwt, {
    secret: process.env.CRYPCIL_AUTH_SECRET??"secretaf",
  });

  server.register(fastifyCors, {
    origin: "*",
    methods: ["GET", "PUT", "POST"],
  });


  server.get("/healthcheck", async function () {
    return { status: "OK" };
  });

  for (const schema of [...userSchemas]) {
    server.addSchema(schema);
  }

  server.addHook("preHandler", (req, reply, next) => {
    req.jwt = server.jwt;
    return next();
  });

  server.register(
    swagger,
    withRefResolver({
      routePrefix: "/docs",
      exposeRoute: true,
      staticCSP: true,
      openapi: {
        info: {
          title: "Crypcil-Auth API",
          description: "API for Crypcil Authentication Microservice using the OAuth ROPC flow",
          version,
        },
      },
    })
  );
  server.register(crypcilRoutes)

  return server;
}

export default buildServer;
