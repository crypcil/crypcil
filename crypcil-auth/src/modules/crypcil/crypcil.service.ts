import {hashPassword} from "../../utils/hash";
import prisma from "../../utils/prisma";
import {CreateUserInput} from "./crypcil.schema";

export async function createUser(input: CreateUserInput) {
  const { password, ...rest } = input;

  const { hash, salt } = hashPassword(password);
  return await prisma.user.create({
    data: {...rest, salt, password: hash},
  });
}

export async function findUserByUsername(username: string) {
  return await prisma.user.findUnique({
    where: {
      username
    }
  })
}

export async function verifyClient(client_id: string, client_secret: string) {
  const client = await prisma.client.findUnique({
    where: {
      client_id
    }
  })
  if(!client) {
    return false
  }
  try {
    return (client.client_secret === client_secret)
  } catch (e) {
    return false
  }
}