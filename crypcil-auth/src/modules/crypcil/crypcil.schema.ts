import {z} from "zod";
import {buildJsonSchemas} from "fastify-zod";

const infoCore = {
  address: z.string().regex(/^0x[a-fA-F0-9]{40}$/g)
}

const publicCredentialCore = {
  username: z
    .string({
      required_error: "Username is required",
      invalid_type_error: "Username must be a string",
    })
}

const credentialCore = {
  ...publicCredentialCore,
  password: z.string({
    required_error: "Password is required",
    invalid_type_error: "Password must be a string",
  }),
};

const createUserSchema = z.object({
  ...infoCore,
  ...credentialCore,
});

const createUserResponseSchema = z.object({
  id: z.number(),
  ...infoCore,
  ...publicCredentialCore
})

const loginSchema = z.object({
  ...credentialCore,
  grant_type: z.literal("password"),
  client_id: z.string({
    required_error: "client_id is required",
    invalid_type_error: "client_id must be a string",
  }),
  client_secret: z.string({
    required_error: "client_secret is required",
    invalid_type_error: "client_secret must be a string",
  }),
})

const loginResponseSchema = z.object({
  access_token: z.string(),
  expires_in: z.number(),
  token_type: z.literal("Bearer"),
})

const signedSchema = z.object({
  ...publicCredentialCore,
  ...infoCore,
  role: z.string(),
  client_id: z.string(),
})

export type CreateUserInput = z.infer<typeof createUserSchema>
export type LoginInput = z.infer<typeof loginSchema>
export type LoginResponse = z.infer<typeof loginResponseSchema>
export type SignedResponse = z.infer<typeof signedSchema>

export const {schemas: userSchemas, $ref} = buildJsonSchemas({
  createUserSchema,
  createUserResponseSchema,
  loginSchema,
  loginResponseSchema,
  signedSchema
})