import {FastifyInstance} from "fastify";
import {registerHandler, loginHandler, verifyHandler} from "./crypcil.controller";
import {$ref} from "./crypcil.schema";

async function crypcilRoutes(server :FastifyInstance) {
  server.post('/register',{
    schema: {
      body: $ref('createUserSchema'),
      response:{
        201:$ref('createUserResponseSchema')
      }
    }
  }, registerHandler)

  server.post('/login', {
    schema: {
      body: $ref('loginSchema'),
      response: {
        201: $ref('loginResponseSchema')
      },
    }
  }, loginHandler);

  server.get("/verify", {
    schema: {
      response: {
        201: $ref('signedSchema')
      }
    }
  }, verifyHandler)
}

export default crypcilRoutes