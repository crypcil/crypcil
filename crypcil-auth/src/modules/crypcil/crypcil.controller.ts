import {FastifyInstance, FastifyReply, FastifyRequest} from "fastify";
import {createUser, findUserByUsername, verifyClient} from "./crypcil.service";
import {CreateUserInput, LoginInput, LoginResponse, SignedResponse} from "./crypcil.schema";
import {verifyPassword} from "../../utils/hash";
import server from "../../server";

export async function registerHandler(
  request: FastifyRequest<{
    Body: CreateUserInput
  }>,
  reply: FastifyReply
) {
  const body = request.body

  try {
    const oldUser = await findUserByUsername(request.body.username)
    if(!!oldUser) {
      return reply.code(500).send({message: `Username ${request.body.username} already exist`})
    }
    const user = await createUser(body)
    return reply.code(201).send(user)
  } catch (e) {
    console.error(e)
    return reply.code(500).send(e)
  }
}

export async function loginHandler(
  request: FastifyRequest<{
    Body: LoginInput;
  }>,
  reply: FastifyReply
){
  const body = request.body
  console.log(body)
  // Verify client id and secret
  if(!await verifyClient(body.client_id, body.client_secret)) {
    return reply.code(401).send({
      message: "Invalid client"
    })
  }

  const user = await findUserByUsername(body.username)
  if(!user) {
    return reply.code(401).send({
      message: `Username ${body.username} not found in the server`
    })
  }

  if(!verifyPassword({
    candidatePassword: body.password,
    salt: user.salt,
    hash: user.password
  })) {
    return reply.code(401).send({
      message: "Wrong password"
    })
  }

  const data: SignedResponse = {
    username: user.username,
    address: user.address,
    role: user.role,
    client_id: body.client_id,
  }
  console.log(data)
  const returnData:LoginResponse = {
    access_token: request.jwt.sign(data, {
      expiresIn: "1h"
    }), expires_in: 3600, token_type: "Bearer"
  }

  return reply.code(200).send(returnData)
}

export async function verifyHandler(
  request: FastifyRequest,
  reply: FastifyReply
) {
  try {
    const jwttoken = request.headers.authorization!
    const TokenArray = jwttoken.split(" ");
    const ok = request.jwt.verify(TokenArray[1])
    if(!ok || TokenArray[0] != "Bearer") {
      return reply.code(401).send({
        "message": "problem with jwt"
      })
    }
    const payload = request.jwt.decode(TokenArray[1])
    return reply.code(201).send(payload)
  } catch (e) {
    console.error(e)
    return reply.code(401).send({
      "message": "error with jwttoken"
    })
  }

}