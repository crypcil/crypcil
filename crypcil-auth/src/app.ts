import buildServer from "./server";
import 'dotenv/config' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
export const server = buildServer();

async function main() {
  try {
    const port = process.env.PORT??3001
    await server.listen(port, "0.0.0.0");
    console.log(`Server ready at http://localhost:${port}`);
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
}

main();
