import grpc
import image_procedure_pb2 as pb
import image_procedure_pb2_grpc


def main():
    channel = grpc.insecure_channel('localhost:50051')
    stub = image_procedure_pb2_grpc.ImageProcedureStub(channel)

    try:
        response = stub.CreateImage(pb.ListOfCurrentPrice)
        print(response)

    # Catch any raised errors by grpc.
    except grpc.RpcError as e:
        print("Error raised: " + e.details())

if __name__ == '__main__':
    main()