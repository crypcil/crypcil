# Crypcil-history

## Installation
Windows:
```shell
python -m venv env
env\Scripts\activate
pip install -r requirements.txt
```

## Tech
* Python
* gRPC

## Features
* Create image

## Documentation
```
// input = (Price, Timestamp)[] 
message ListOfCurrentPrice {
    message CurrentPrice {
    repeated string price = 1;
    repeated string timestamp = 2;
    }
    repeated CurrentPrice currentPrices = 1;
}

// output = String image of a line graph
message Image {
    string image = 1;
}
```

### Service:
```
CreateImage(ListOfCurrentPrice) returns (Image) {
        option = {
            post: "/api/v1/image"
            body: "*"
        };
}
```