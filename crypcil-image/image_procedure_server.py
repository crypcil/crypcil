from concurrent import futures
import grpc
import image_procedure_pb2
import image_procedure_pb2_grpc
import pandas as pd
import plotly.express as px

def get_dataframe(list):
    df = pd.DataFrame(list)
    df.columns = ['Price', 'Timestamp']
    return df

class ImageProcedureServicer(image_procedure_pb2_grpc.ImageProcedureServicer):
    def CreateImage(self, request, context):
        fig = px.line(get_dataframe(request.list), x='Timestamp', y="Price")
        img_bytes = fig.to_image(format="jpg")
        return img_bytes

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    image_procedure_pb2_grpc.add_ImageProcedureServicer_to_server(
        ImageProcedureServicer(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()

if __name__ == '__main__':
    serve()