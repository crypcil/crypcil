# Crypcil-returns
## Up-ing the DB

crypcil-returns uses TimescaleDB, an extension of PostgreSQL that allows us to easily and quickly work with time-series data.
First up, make sure your OS has docker set up if you want to deploy the db in your localhost. Else, should be a good idea to deploy a virtual instance.

But it's kinda tricky to set up. Here's how you set up on local:

```bash
docker-compose up -d timescaledb
npx mikro-orm migration:up
npm run seed
```

The database should be running on `jdbc:postgresql://localhost:5432/crypcil` with username `postgres` and password `crypcildex`.

## Running backend in local

To run the app, copy `.env.example` to `.env`, then use the bash command:

```bash
npm run dev
```

To make sure it is running well, checkout `localhost:8080`, or wherever you put the port of your app running in.
