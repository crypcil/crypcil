import {EntityManager} from "@mikro-orm/postgresql";
import {FastifyPluginAsync} from "fastify";
import fp from "fastify-plugin";
import {Wallet} from "../db/entities/Wallet";
import {WalletTransaction} from "../db/entities/WalletTransaction";
import {getAllCoinsAmountData, getReturnData} from "../service/returns";
import {getAndParseTransactions, getMatchingPrice} from "../service/track-transaction";
import {getCurrentPrice} from "../service/returns";
import {TOKENS} from "../db/seed/token";
import {walletTransactionSchema} from "../schemas/wallet";

const trackWallet = async (address: string, em: EntityManager) => {
  const walletRepository = em.getRepository(Wallet);

  let wallet = await walletRepository.findOne({
    address,
  });
  if (!wallet) {
    wallet = new Wallet(address, new Date());
    await em.persistAndFlush(wallet);
  }

  const lastTransactionBlockNumber = Number(wallet.last_trx_block_number);

  // The + 1 is to make sure we get the latest transactions, if it doesn't exist, use 0
  const trxs = await getAndParseTransactions(
    address,
    lastTransactionBlockNumber ? lastTransactionBlockNumber + 1 : 0
  );

  const coinPrice = await getMatchingPrice(trxs);
  const walletTrxsToPersist = trxs.map((trx, index) => {
      const price = coinPrice[index].price
      return new WalletTransaction(
        trx.hash,
        trx.from,
        trx.to,
        wallet as Wallet,
        trx.timeStamp,
        trx.type,
        TOKENS[trx.tokenSymbol].id,
        trx.value,
        trx.value * (price ?? 0),
        trx.blockNumber,
      );
  });

  await em.persistAndFlush(walletTrxsToPersist);

  if (trxs.length > 0) {
    const lastIndex = trxs.length - 1;
    wallet.last_trx_time = trxs[lastIndex].timeStamp;
    wallet.last_trx_hash = trxs[lastIndex].hash;
    wallet.last_trx_block_number = trxs[lastIndex].blockNumber;
    await em.persistAndFlush(wallet);
  }

  return wallet;
};

export const wallet: FastifyPluginAsync = fp(async (instance, options) => {
  instance.log.info(options);

  instance.route<{ Params: { address: string } }>({
    method: "GET",
    url: "/wallet/:address/track",
    handler: async (request, reply) => {
      const address = request.params.address;
      const em = request.orm.em;

      const wallet = await trackWallet(address, em);

      reply.send({
        status: "success",
        data: wallet,
      });
    },
  });

  instance.route<{ Params: { address: string } }>({
    method: "GET",
    url: "/wallet/:address/returns",
    handler: async (request, reply) => {
      const address = request.params.address;
      const em = request.orm.em;

      const walletTransactionRepository = em.getRepository(WalletTransaction);

      let wallet = await trackWallet(address, em);

      const walletTransactions = await walletTransactionRepository.find(
        {
          wallet,
        }
      );
      const currentPriceRecord = await getCurrentPrice()

      reply.send({
        status: "success",
        data: {
          ...getReturnData(walletTransactions, currentPriceRecord),
          ...wallet,
        },
      });
    },
  });

  instance.route<{ Params: { address: string } }>({
    method: "GET",
    url: "/wallet/:address/amount",
    handler: async (request, reply) => {
      const address = request.params.address;
      const em = request.orm.em;

      const wallet = await trackWallet(address, em);
      const walletTransactionRepository = em.getRepository(WalletTransaction);
      ``;
      const walletTransactions = await walletTransactionRepository.find(
        {
          wallet,
        },
      );

      reply.send({
        status: "success",
        data: {
          ...getAllCoinsAmountData(walletTransactions),
          ...wallet,
        },
      });
    },
  });
  instance.route<{ Params: { address: string } }>({
    method: "GET",
    url: "/wallet/:address/history",
    handler: async (request, reply) => {
      const address = request.params.address;
      const em = request.orm.em;

      const wallet = await trackWallet(address, em);
      const walletTransactionRepository = em.getRepository(WalletTransaction);
      ``;
      const walletTransactions = await walletTransactionRepository.find(
        {
          wallet,
        },
      );
      reply.send({
        status: "success",
        data: walletTransactions,
      });
    },
  });
});
