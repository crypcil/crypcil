export const hargaPast = {
  body: {
    type: 'object',
    required: "data",
    properties: {
      data: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            timestamp: {default: '1653486554000', type: 'string'},
            coin_id: {default: 'tether', type: 'string'}
          },
        },
        minItems: 2,
      }
    }
  }
  ,
  response: {
    200: {
      type: 'object',
      description: 'Response success will return array of prices 2022-05-25\'s prices',
      properties: {
        status: {type: 'string'},
        prices: {
          type: 'array', items: {
            type: 'object',
            properties: {
              "time": {type: "string", default: "2022-05-25T13:49:00.897Z"},
              "coin": {type: "string", default: "tether"},
              "price": {type: "number", default: 0.998677},
              "market_cap": {type: "number", default: 73369403837.45961},
              "total_volume": {type: "number", default: 44455165115.0752}
            }
          },
          minItems: 2
        },
      }
    },

  },
  // params: {
  //   type: 'object',
  //   properties: {
  //     name: { type: 'string' }
  //   }
  // }
}

export const walletTransactionSchema = {
  response: {
    200: {
      type: 'object',
      description: 'Response success will return of complete daily prices',
      properties: {
        status: {type: 'string'},
        prices: {
          type: 'array', items: {
            type: 'number',
            default: 500.34
          },
          minItems: 2
        },
      }
    },

  },
  // params: {
  //   type: 'object',
  //   properties: {
  //     name: { type: 'string' }
  //   }
  // }
}