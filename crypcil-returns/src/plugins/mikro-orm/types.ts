/* eslint-disable no-unused-vars */
import { MikroORM, Options } from "@mikro-orm/core";
import { PostgreSqlDriver } from "@mikro-orm/postgresql";

export declare namespace fastifyMikroOrm {
  type Awaited<T> = T extends PromiseLike<infer U> ? Awaited<U> : T;

  type FastifyMikroOrmOptions = {
    orm?: MikroORM<PostgreSqlDriver>;
  };

  type MikroORMPluginOptions = Options<PostgreSqlDriver> &
    FastifyMikroOrmOptions;
}

declare module "fastify" {
  interface FastifyInstance {
    orm: fastifyMikroOrm.Awaited<MikroORM<PostgreSqlDriver>>;
  }
  interface FastifyRequest {
    orm: fastifyMikroOrm.Awaited<MikroORM<PostgreSqlDriver>>;
  }
}
