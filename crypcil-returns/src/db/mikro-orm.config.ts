import { Options } from "@mikro-orm/core";
import { PostgreSqlDriver } from "@mikro-orm/postgresql";
import { TsMorphMetadataProvider } from "@mikro-orm/reflection";
import { env } from "../environment";

const ormConfig: Options<PostgreSqlDriver> = {
  clientUrl: env.DB_CONNECTION_URL,
  type: "postgresql",
  entities: ["./build/db/entities"],
  entitiesTs: ["./src/db/entities"],
  migrations: {
    pathTs: "./src/db/migrations",
    path: "./build/db/migrations",
  },
  metadataProvider: TsMorphMetadataProvider,
};

export default ormConfig;
