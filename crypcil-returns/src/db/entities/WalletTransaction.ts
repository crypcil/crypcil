import {
  Entity,
  Enum,
  IdentifiedReference,
  ManyToOne,
  PrimaryKey,
  PrimaryKeyType,
  Property,
  types,
  wrap
} from "@mikro-orm/core";
import { CrypcilBaseEntity } from "./BaseEntity";
import { Wallet } from "./Wallet";

@Entity()
export class WalletTransaction extends CrypcilBaseEntity {

  [PrimaryKeyType]?: [string, IdentifiedReference<Wallet>, WalletTransactionType];

  @PrimaryKey()
  hash!: string;

  @Property()
  from!: string;

  @Property()
  to!: string;

  @ManyToOne({primary: true})
  wallet!: IdentifiedReference<Wallet>;

  @Property({ columnType: "timestamptz", type: types.datetime })
  time!: Date;

  @Enum({primary: true, items: () => WalletTransactionType})
  type!: WalletTransactionType;

  @Property()
  coin_id!: string;

  @Property({ type: types.double })
  amount!: number;

  @Property({ type: types.double })
  worth!: number;

  @Property()
  note?: string;

  @Property()
  blockNumber?: string;

  constructor(
    hash: string,
    from: string,
    to: string,
    wallet: Wallet,
    time: Date,
    type: WalletTransactionType,
    coin_id: string,
    amount: number,
    worth: number,
    blockNumber: string,
    note?: string
  ) {
    super();
    this.hash = hash;
    this.from = from;
    this.to = to;
    this.wallet = wrap(wallet).toReference();
    this.time = time;
    this.type = type;
    this.coin_id = coin_id;
    this.amount = amount;
    this.worth = worth;
    this.note = note;
    this.blockNumber = blockNumber;
  }
}

export enum WalletTransactionType {
  IN = "in",
  OUT = "out",
}
