import { Entity } from "@mikro-orm/core";

@Entity({ abstract: true, schema: "crypcil" })
export abstract class CrypcilBaseEntity {}
