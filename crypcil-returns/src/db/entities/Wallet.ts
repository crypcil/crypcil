import { Entity, PrimaryKey, Property, types } from "@mikro-orm/core";
import { CrypcilBaseEntity } from "./BaseEntity";

@Entity()
export class Wallet extends CrypcilBaseEntity {
  @PrimaryKey({ type: types.text })
  address!: string;

  @Property({ type: types.text })
  last_trx_hash?: string;

  @Property({ type: types.text })
  last_trx_block_number?: string;

  @Property({ columnType: "timestamptz", type: types.datetime })
  last_trx_time?: Date;

  @Property({ columnType: "timestamptz", type: types.datetime })
  track_start_time?: Date;

  constructor(address: string, track_start_time: Date) {
    super();
    this.address = address;
    this.track_start_time = track_start_time;
  }
}
