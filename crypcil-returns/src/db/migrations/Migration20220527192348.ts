import { Migration } from '@mikro-orm/migrations';

export class Migration20220527192348 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table "wallet" ("address" text not null, "last_trx_hash" text null, "last_trx_block_number" text null, "last_trx_time" timestamptz null, "track_start_time" timestamptz null);');
    this.addSql('alter table "wallet" add constraint "wallet_pkey" primary key ("address");');

    this.addSql('create table "wallet_transaction" ("hash" varchar(255) not null, "wallet_address" text not null, "type" text check ("type" in (\'in\', \'out\')) not null, "from" varchar(255) not null, "to" varchar(255) not null, "time" timestamptz not null, "coin_id" varchar(255) not null, "amount" double precision not null, "worth" double precision not null, "note" varchar(255) null, "block_number" varchar(255) null);');
    this.addSql('alter table "wallet_transaction" add constraint "wallet_transaction_pkey" primary key ("hash", "wallet_address", "type");');

    this.addSql('alter table "wallet_transaction" add constraint "wallet_transaction_wallet_address_foreign" foreign key ("wallet_address") references "wallet" ("address") on update cascade;');
  }

  async down(): Promise<void> {
    this.addSql('alter table "wallet_transaction" drop constraint "wallet_transaction_wallet_address_foreign";');

    this.addSql('drop table if exists "wallet" cascade;');

    this.addSql('drop table if exists "wallet_transaction" cascade;');
  }

}
