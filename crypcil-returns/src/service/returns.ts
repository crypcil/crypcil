import _ from "lodash";
import {
  WalletTransaction,
  WalletTransactionType,
} from "../db/entities/WalletTransaction";
import axios from "axios";

export type CurrentCoinPrice = {
  usd: number;
};

export const getReturnData = (
  trxs: WalletTransaction[],
  currentPriceRecord: Record<string, CurrentCoinPrice>
) => {
  const returns = trxs.map((trx) => {
    const trxCoinId = trx.coin_id;
    const currentPrice = currentPriceRecord[trxCoinId];
    if (!currentPrice) {
      throw new Error(`No current price for ${trxCoinId}`);
    }
    const returnValue = trx.amount * currentPrice.usd - trx.worth;
    return trx.type == WalletTransactionType.IN ? returnValue : -returnValue;
  });

  const holdings = trxs.map((trx) => {
    const trxCoinId = trx.coin_id;
    const currentPrice = currentPriceRecord[trxCoinId];
    if (!currentPrice) {
      throw new Error(`No current price for ${trxCoinId}`);
    }
    const returnValue = trx.amount * currentPrice.usd;
    return trx.type == WalletTransactionType.IN ? returnValue : -returnValue;
  });

  return { returns: _.sum(returns), holdings: _.sum(holdings) };
};

export const getAllCoinsAmountData = (trxs: WalletTransaction[]) => {
  const allCoinsAmount = new Map<string, number>();
  trxs.map((trx) => {
    const coinAmount = trx.amount;
    const trxCoinId = trx.coin_id;
    const oldAmount = allCoinsAmount.get(trxCoinId) ?? 0;
    const newAmount =
      oldAmount +
      (trx.type == WalletTransactionType.IN ? coinAmount : -coinAmount);

    allCoinsAmount.set(trxCoinId, newAmount);
  });

  return { allCoinsAmount: Object.fromEntries(allCoinsAmount) };
};

export const getCurrentPrice = async () => {
  const price = await axios.get(`${process.env.TRACK_URL}/track/harga`);
  return price.data.data;
}