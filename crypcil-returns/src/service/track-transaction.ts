import {BigNumber} from "ethers";
import {formatUnits} from "ethers/lib/utils";
import got from "got";
import {WalletTransactionType} from "../db/entities/WalletTransaction";
import {TOKENS} from "../db/seed/token";
import axios from "axios";

type NormalTransaction = {
  blockNumber: string;
  timeStamp: string;
  hash: string;
  nonce: string;
  blockHash: string;
  transactionIndex: string;
  from: string;
  to: string;
  value: string;
  gas: string;
  gasPrice: string;
  isError: string;
  txreceipt_status: string;
  input: string;
  contractAddress: string;
  cumulativeGasUsed: string;
  gasUsed: string;
  confirmations: string;
};

type TokenTransferEvent = {
  blockNumber: string;
  timeStamp: string;
  hash: string;
  nonce: string;
  blockHash: string;
  from: string;
  contractAddress: string;
  to: string;
  value: string;
  tokenName: string;
  tokenSymbol: string;
  tokenDecimal: string;
  transactionIndex: string;
  gas: string;
  gasPrice: string;
  gasUsed: string;
  cumulativeGasUsed: string;
  input: string;
  confirmations: string;
};

type NormalTransactionData = {
  status: string;
  message: string;
  result: NormalTransaction[];
};

type TokenTransactionData = {
  status: string;
  message: string;
  result: TokenTransferEvent[];
};

type Transaction = {
  blockNumber: string;
  timeStamp: Date;
  hash: string;
  from: string;
  to: string;
  value: number;
  tokenName: string;
  tokenSymbol: string;
  tokenDecimal: number;
  type: WalletTransactionType;
};

const getNormalTransactions = async (
  address: string,
  lastTransactionBlockNumber: number
) => {
  const url = `${process.env.HISTORY_URL}/history/normal/${address}/${lastTransactionBlockNumber}`;
  const response = (await got.get(url).json()) as NormalTransaction[]
  const transactions: Transaction[] = response
    .filter((trx) => trx.value !== "0")
    .map(({ blockNumber, timeStamp, hash, from, to, value }) => ({
      blockNumber,
      timeStamp: new Date(Number(timeStamp) * 1000),
      hash,
      from,
      to,
      value: Number(formatUnits(BigNumber.from(value))),
      tokenName: "Binance Coin",
      tokenSymbol: "BNB",
      tokenDecimal: 18,
      type:
        address.toLowerCase() === from.toLowerCase()
          ? WalletTransactionType.OUT
          : WalletTransactionType.IN,
    }));
  return transactions;
};

const getTokenTransactions = async (
  address: string,
  lastTransactionBlockNumber: number
) => {
  const url = `${process.env.HISTORY_URL}/history/token/${address}/${lastTransactionBlockNumber}`;
  const transactions: Transaction[] = (await got.get(url).json() as Transaction[]).map(
      ({
        blockNumber,
        timeStamp,
        hash,
        from,
        to,
        value,
        tokenName,
        tokenSymbol,
        tokenDecimal,
      }) => ({
        blockNumber,
        timeStamp: new Date(Number(timeStamp) * 1000),
        hash,
        from,
        to,
        value: Number(formatUnits(BigNumber.from(value), Number(tokenDecimal))),
        tokenName,
        tokenSymbol,
        tokenDecimal: Number(tokenDecimal),
        type:
          address.toLowerCase() === from.toLowerCase()
            ? WalletTransactionType.OUT
            : WalletTransactionType.IN,
      })
    )
    .filter(({ tokenSymbol }) => Object.keys(TOKENS).includes(tokenSymbol));
  return transactions;
};

export const getAndParseTransactions = async (
  address: string,
  lastTransactionBlockNumber: number
) => {
  // should probably refactor this into concurrent requests
  const normalTransactions = await getNormalTransactions(
    address,
    lastTransactionBlockNumber
  );
  const tokenTransactions = await getTokenTransactions(
    address,
    lastTransactionBlockNumber
  );
  const allTransactions = [...normalTransactions, ...tokenTransactions];
  allTransactions.sort((a, b) => a.timeStamp.getTime() - b.timeStamp.getTime());
  return allTransactions;
};

export const getMatchingPrice = async (
  transaction: Transaction[],
) => {
    const queryData = transaction.map((val) => {
      return {timestamp: val.timeStamp.getTime(), coin_id: TOKENS[val.tokenSymbol].id}
    })
    const payload = { data: queryData }
    const res = await axios.post(`${process.env.TRACK_URL}/track/harga_past`, payload);

    let data = res.data.prices;
    return data;
};
