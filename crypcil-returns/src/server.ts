import fastify from "fastify";
import ormConfig from "./db/mikro-orm.config";
import fastifyCron from "fastify-cron";
import fastifyCors from "fastify-cors";
import {withRefResolver} from "fastify-zod";

import {env} from "./environment";
import mikroORMPlugin from "./plugins/mikro-orm";

import {ping} from "./routes/ping";
import swagger from "@fastify/swagger";
import {wallet} from "./routes/wallet";

export const server = fastify({
  logger: env.LOGGER && {
    level: "info",
    prettyPrint: env.NODE_ENV === "development",
  },
});

server.register(fastifyCors, {
  origin: "*",
  methods: ["GET", "PUT", "POST"],
});

server.register(ping, {});

server.register(wallet, {});

server.register(mikroORMPlugin, ormConfig);

server.register(
  swagger, {
    routePrefix: '/docs',
    swagger: {
      info: {
        title: 'Test swagger',
        description: 'Testing the Fastify swagger API',
        version: '0.1.0'
      },
      externalDocs: {
        url: 'https://swagger.io',
        description: 'Find more info here'
      },
      host: 'localhost',
      schemes: ['http'],
      consumes: ['application/json'],
      produces: ['application/json'],
    },
    uiConfig: {
      docExpansion: 'full',
      deepLinking: false
    },
    uiHooks: {
      onRequest: function (request, reply, next) {
        next()
      },
      preHandler: function (request, reply, next) {
        next()
      }
    },
    staticCSP: true,
    transformStaticCSP: (header) => header,
    exposeRoute: true
  }
);


server.ready(async () => {
  server.log.info("Ready!");
  console.log(
    `Server is ready! Logging is ${env.LOGGER ? "enabled" : "disabled"}!`
  );
});
