# Crypcil-history

Windows:
```shell
python -m venv env
env\Scripts\activate
pip install -r requirements.txt

uvicorn main:app --reload
```
Service will run in port 8000 by default, open your local host and please see url /docs for documentation.

## Tech
* Python
* FastAPI

## Features
* Get all transaction history of a wallet
* Get last transaction of a wallet