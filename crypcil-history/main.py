from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from functools import lru_cache
import config
import requests

app = FastAPI()

origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Settings
@lru_cache()
def get_settings():
    return config.Settings()

# Helper function
def get_api_key_bscscan():
    settings = get_settings()
    return settings.api_key_bscscan

def call_history_api(wallet_address):
    api_key = str(get_api_key_bscscan())
    url = "https://api.bscscan.com/api?module=account&action=txlist&address={}&sort=desc&apikey={}&page=1&offset=10" \
            .format(wallet_address, api_key)
    response = requests.get(url).json()["result"]
    return response


def call_history_api_with_block_normal(wallet_address, block_number):
    api_key = str(get_api_key_bscscan())
    print(api_key)
    url = f"https://api.bscscan.com/api?module=account&startblock={block_number}&action=txlist&address={wallet_address}&sort=desc&apikey={api_key}"
    print(url)
    response = requests.get(url).json()["result"]
    return response

def call_history_api_with_block_token(wallet_address, block_number):
    api_key = str(get_api_key_bscscan())
    url = f"https://api.bscscan.com/api?module=account&startblock={block_number}&action=tokentx&address={wallet_address}&sort=desc&apikey={api_key}"
    response = requests.get(url).json()["result"]
    return response

# API
@app.get("/history/transactions/{wallet_address}")
def get_transaction_history(wallet_address: str):
    history = call_history_api(wallet_address)
    return history

@app.get("/history/normal/{wallet_address}/{block_number}")
async def normal_transaction_history(wallet_address, block_number):
    history = call_history_api_with_block_normal(wallet_address, block_number)
    return history


@app.get("/history/token/{wallet_address}/{block_number}")
async def token_transaction_history(wallet_address, block_number):
    history = call_history_api_with_block_token(wallet_address, block_number)
    return history


@app.get("/history/last_transaction/{wallet_address}")
def get_last_transaction(wallet_address: str):
    history = call_history_api(wallet_address)
    last = len(history)-1
    return history[last]
