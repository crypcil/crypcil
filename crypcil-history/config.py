from pydantic import BaseSettings

class Settings(BaseSettings):
    api_key_bscscan: str

    class Config:
        env_file = ".env"