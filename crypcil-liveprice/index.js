require("dotenv").config();

const winston = require("winston");
const {
  LogstashTransport,
  createLogger,
} = require("winston-logstash-transport");

const logger = createLogger(null, {
  logstash: {
    host: "b69a60bf-5eb1-4395-8355-56bb724b1d21-ls.logit.io",
    port: 10006,
  },
  transports: [new winston.transports.Console()],
  application: "liveprice-tracker",
});

const amqp = require("amqplib/callback_api");
const trackToken = require("./liveprice");

const amqpOptions = {
  credentials: require("amqplib").credentials.plain(
    process.env.QUEUE_USERNAME,
    process.env.QUEUE_PASSWORD
  ),
};

amqp.connect(process.env.QUEUE_URL, amqpOptions, function (error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function (error1, channel) {
    if (error1) {
      throw error1;
    }
    var queue = "liveprice_queue";

    channel.assertQueue(queue, {
      durable: false,
    });
    channel.prefetch(1);
    logger.info(" [x] Awaiting RPC requests");
    channel.consume(queue, async function reply(msg) {
      const token = msg.content.toString();

      logger.info(` [.] trackToken(${token})`);

      const prices = await trackToken(token, 1 * 1000);

      const r = JSON.stringify(prices);

      logger.info(" [x] Sending results");

      channel.sendToQueue(msg.properties.replyTo, Buffer.from(r.toString()), {
        correlationId: msg.properties.correlationId,
      });

      channel.ack(msg);
    });
  });
});
