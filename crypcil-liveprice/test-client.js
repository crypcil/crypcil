require("dotenv").config();

const amqp = require("amqplib/callback_api");

const amqpOptions = {
  credentials: require("amqplib").credentials.plain(
    process.env.QUEUE_USERNAME,
    process.env.QUEUE_PASSWORD
  ),
};

amqp.connect(process.env.QUEUE_URL, amqpOptions, function (error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function (error1, channel) {
    if (error1) {
      throw error1;
    }
    channel.assertQueue(
      "",
      {
        exclusive: true,
      },
      function (error2, q) {
        if (error2) {
          throw error2;
        }
        var correlationId = generateUuid();

        const token = "TKO";

        console.log(` [x] Requesting trackToken(${token})`);

        channel.consume(
          q.queue,
          function (msg) {
            if (msg.properties.correlationId == correlationId) {
              console.log(" [.] Got %s", msg.content.toString());
              setTimeout(function () {
                connection.close();
                process.exit(0);
              }, 500);
            }
          },
          {
            noAck: true,
          }
        );

        channel.sendToQueue("liveprice_queue", Buffer.from(token.toString()), {
          correlationId: correlationId,
          replyTo: q.queue,
        });
      }
    );
  });
});

function generateUuid() {
  return (
    Math.random().toString() +
    Math.random().toString() +
    Math.random().toString()
  );
}
