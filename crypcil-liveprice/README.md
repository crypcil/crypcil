# LivePrice service for Crypcil

This service utilizes `simple-pancakeswap-sdk` and its multicall capability to live query the Binance Smart Chain to get live 'price' (exchange price to USDT).

## Message Queue

This service adopts the RPC pattern described in RabbitMQ Docs in order to consume track requests. The default track interval is 5 minutes.

As such, to 'call' the service, refer to the demo code below:

```
amqp.connect(process.env.QUEUE_URL, amqpOptions, function (error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function (error1, channel) {
    if (error1) {
      throw error1;
    }
    channel.assertQueue(
      "",
      {
        exclusive: true,
      },
      function (error2, q) {
        if (error2) {
          throw error2;
        }
        var correlationId = generateUuid();

        const token = "TKO"; // TOKEN SYMBOL REQUESTED

        console.log(` [x] Requesting trackToken(${token})`);

        channel.consume(
          q.queue,
          function (msg) {
            if (msg.properties.correlationId == correlationId) {
              console.log(" [.] Got %s", msg.content.toString());
              setTimeout(function () {
                connection.close();
                process.exit(0);
              }, 500);
            }
          },
          {
            noAck: true,
          }
        );

        channel.sendToQueue("liveprice_queue", Buffer.from(token.toString()), {
          correlationId: correlationId,
          replyTo: q.queue,
        });
      }
    );
  });
});
```
