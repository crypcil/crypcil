import { insertDailyCoinData } from "../../src/handlers/insertDailyCoinData";
import { MikroORM, Reference } from "@mikro-orm/core";

describe("insertDailyCoinData", () => {
  // before each reset mock
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("should insert data", async () => {
    // mock micro-orm
    const orm = {
      em: {
        fork: jest.fn().mockReturnValue({
          persist: jest.fn().mockReturnValue([]),
          flush: jest.fn().mockReturnValue([]),
        }),
      },
      close: jest.fn().mockReturnValue([]),
    };

    // mock client
    const client = {
      simplePrice: jest.fn().mockReturnValue({
        idr_market_cap: 137434342325.91925,
        idr_24h_vol: 2196147804.766063,
        idr: 1075.91,
        last_updated_at: 1648983612,
      }),
    };

    jest.spyOn(MikroORM, "init").mockImplementation(() => orm as any);
    jest.spyOn(Reference, "createFromPK").mockImplementation(
      () =>
        ({
          id: "pacoca",
        } as any)
    );

    // mock CoinGeckoClient
    jest.mock("coingecko-api-v3-axios", () => ({
      CoinGeckoClient: jest.fn().mockReturnValue(client),
    }));

    // mock CoinPrice
    jest.mock("../../src/db/entities/CoinPrice", () => ({
      CoinPrice: jest.fn().mockImplementation(() => ({
        id: "",
        coin: "",
        price: 0,
        market_cap: 0,
        total_volume: 0,
        time: new Date(),
      })),
    }));

    await insertDailyCoinData();

    expect(orm.em.fork).toHaveBeenCalledTimes(1);
    expect(orm.em.fork).toHaveBeenCalledWith();

    expect(orm.em.fork().persist).toHaveBeenCalledTimes(1);
  });
});
