import {MikroORM} from "@mikro-orm/core";
import {CoinGeckoClient} from "coingecko-api-v3-axios";
import {CoinPrice} from "../db/entities/CoinPrice";
import ormConfig from "../db/mikro-orm.config";
import {TOKEN_IDS_CSV} from "../db/seed/token";
import {setPrice} from "../db/dailyPriceCache";

const client = new CoinGeckoClient();

export const insertDailyCoinData = async (): Promise<void> => {
  const res = await client.simplePrice({
    ids: TOKEN_IDS_CSV,
    vs_currencies: "usd",
    include_24hr_vol: true,
    include_market_cap: true,
    include_last_updated_at: true,
    include_24hr_change: true,
  });
  // Set to info priceState
  setPrice({price: res});

  // insert to db CoinPrice

  const orm = await MikroORM.init(ormConfig);
  const em = orm.em.fork();

  // insert multiple rows
  /*
	  pacoca: {
    usd: 1075.91,
    usd_market_cap: 137434342325.91925,
    usd_24h_vol: 2196147804.766063,
    last_updated_at: 1648983612
  },
	*/
  const now = new Date();
  // get timestamp of this date
  const coinPrice = Object.entries(res).map(([id, data]) => ({
    coin: id,
    market_cap: data.usd_market_cap,
    price: data.usd,
    time: now,
    total_volume: data.usd_24h_vol,
  }));

  const toPersist = coinPrice.map(
    (e) => new CoinPrice(e.time, e.coin, e.price, e.market_cap, e.total_volume)
  );

  em.persist(toPersist);
  await em.flush();
  await orm.close();
};
