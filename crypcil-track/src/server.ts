import fastify, { FastifyLoggerInstance } from "fastify";
import ormConfig from "./db/mikro-orm.config";
import fastifyCron from "fastify-cron";
import fastifyCors from "fastify-cors";
import { withRefResolver } from "fastify-zod";

import { env } from "./environment";
import mikroORMPlugin from "./plugins/mikro-orm";
import amqpPlugin from "./plugins/amqp/index";

import { ping } from "./routes/ping";
import { insertDailyCoinData } from "./handlers/insertDailyCoinData";
import { info } from "./routes/info";
import swagger from "@fastify/swagger";
import winston from "winston";

import WinstonLogstash from "winston3-logstash-transport";

const { format, transports } = winston;

var initLogger = (appname) => {
  winston.loggers.add("default", {
    level: "info",
    levels: Object.assign(
      { fatal: 0, warn: 4, trace: 7 },
      winston.config.syslog.levels
    ),
    format: format.combine(format.splat(), format.json()),
    defaultMeta: {
      service: appname + "_" + (process.env.NODE_ENV || "development"),
    },
    transports: [
      new WinstonLogstash({
        host: "b69a60bf-5eb1-4395-8355-56bb724b1d21-ls.logit.io",
        port: 10004,
        mode: "tcp",
      }),
    ],
  });

  const logger = winston.loggers.get("default");

  if (env.NODE_ENV !== "production") {
    logger.add(
      new transports.Console({
        format: format.simple(),
        handleExceptions: true,
      })
    );
  }

  process.on("uncaughtException", function (err) {
    console.log("UncaughtException processing: %s", err);
  });

  logger.child = function () {
    return winston.loggers.get("default");
  };

  // logger.fatal = function (msg, meta) {};

  // logger.trace = function (msg, meta) {};

  return logger as unknown as FastifyLoggerInstance;
};

export const server = fastify({
  logger: initLogger("crypcil-track"),
});

server.register(fastifyCors, {
  origin: "*",
  methods: ["GET", "PUT", "POST"],
});
server.register(mikroORMPlugin, ormConfig);
server.register(fastifyCron, {
  jobs: [
    {
      name: "insertDailyCoinData",
      cronTime: "* * * * *", // every minute
      onTick: async (server) => {
        await insertDailyCoinData();
      },
      start: true, // Start job immediately
    },
  ],
});

server.register(amqpPlugin, {
  url: env.QUEUE_URL,
});

server.register(ping, {});

server.register(swagger, {
  routePrefix: "/docs",
  swagger: {
    info: {
      title: "Crypcil-Track",
      description: "Swagger Crypcil Track",
      version: "0.1.0",
    },
    externalDocs: {
      url: "https://swagger.io",
      description: "Find more info here",
    },
    host: "localhost",
    schemes: ["http"],
    consumes: ["application/json"],
    produces: ["application/json"],
  },
  uiConfig: {
    docExpansion: "full",
    deepLinking: false,
  },
  uiHooks: {
    onRequest: function (request, reply, next) {
      next();
    },
    preHandler: function (request, reply, next) {
      next();
    },
  },
  staticCSP: true,
  transformStaticCSP: (header) => header,
  exposeRoute: true,
});

server.register(info, {});

server.ready(async () => {
  server.log.info("Ready!");
  server.log.info(
    `Server is ready! Logging is ${env.LOGGER ? "enabled" : "disabled"}!`
  );
});
