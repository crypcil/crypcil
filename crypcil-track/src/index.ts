import { server } from "./server";
import { env } from "./environment";

server.listen(env.PORT, "0.0.0.0", (err) => {
  if (err) {
    server.log.error(err);
    process.exit(1);
  }
});
