import { MikroORM } from "@mikro-orm/core";
import { PostgreSqlDriver } from "@mikro-orm/postgresql";
import ormConfig from "../db/mikro-orm.config";
import {
  getAndParseTransactions,
  getMatchingPrice
} from "../service/track-transaction";
async function seed() {
  // initialize mikro-orm
  const orm = await MikroORM.init<PostgreSqlDriver>(ormConfig);

  // fork em
  const em = orm.em.fork();

  const trxs = await getAndParseTransactions(
    "0xF8F51298ffE480856bb3e4b0Ae1dE4bf97E5CAD4",
    0
  );
  const z = await getMatchingPrice(trxs[1], em);

  await orm.close();

  return true;
}

seed();
