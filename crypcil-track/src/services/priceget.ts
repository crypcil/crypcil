import {EntityManager} from "@mikro-orm/postgresql";
import {CoinPrice} from "../db/entities/CoinPrice";

export const getMatchingPrice = async (
  coinId: string,
  timestamp: Date,
  em: EntityManager
) => {
  const qb = em
    .qb(CoinPrice)
    .where({coin_id: coinId})
    .orderBy({
      [`abs(extract(epoch from time - timestamp '${timestamp.toISOString()}'))`]:
        "ASC",
    })
    .limit(1);

  const price: CoinPrice[] = await qb.getResult();
  if (price.length === 0) {
    return undefined;
  } else {
    return price[0];
  }
};
