import {insertDailyCoinData} from "../handlers/insertDailyCoinData";
import _ from "lodash";

let priceState = {}

export const setPrice = ({price}: { price: any }) => {
  priceState = price;
}

export const getPrice = async () => {
  if (_.isEmpty(priceState)) {
    return await insertDailyCoinData().then(()=>{
      return priceState;
    })
  }else return priceState
}