import {
  Entity,
  IdentifiedReference,
  ManyToOne,
  PrimaryKey,
  PrimaryKeyType,
  Property,
  Reference,
  types,
} from "@mikro-orm/core";
import { CrypcilBaseEntity } from "./BaseEntity";
import { Coin } from "./Coin";

@Entity()
export class CoinPrice extends CrypcilBaseEntity {
  @PrimaryKey({ columnType: "timestamptz", type: types.datetime })
  time!: Date;

  @ManyToOne({ primary: true })
  coin!: IdentifiedReference<Coin>;

  [PrimaryKeyType]?: [Date, string];

  @Property({ type: types.double })
  price!: number;

  @Property({ type: types.double })
  market_cap?: number;

  @Property({ type: types.double })
  total_volume!: number;

  constructor(
    time: Date,
    coin: string,
    price: number,
    market_cap: number,
    total_volume: number
  ) {
    super();
    this.time = time;
    this.coin = Reference.createFromPK(Coin, coin);
    this.price = price;
    this.market_cap = market_cap ?? 0;
    this.total_volume = total_volume;
  }
}
