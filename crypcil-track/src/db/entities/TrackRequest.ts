import {
  Entity,
  IdentifiedReference,
  ManyToOne,
  PrimaryKey,
  PrimaryKeyType,
  Property,
  Reference,
  types,
} from "@mikro-orm/core";
import { CrypcilBaseEntity } from "./BaseEntity";
import { Coin } from "./Coin";

@Entity()
export class TrackRequest extends CrypcilBaseEntity {
  @PrimaryKey({ columnType: "timestamptz", type: types.datetime })
  time!: Date;

  @ManyToOne({ primary: true })
  coin!: IdentifiedReference<Coin>;

  [PrimaryKeyType]?: [Date, string];

  @Property({ type: types.string })
  status!: string;

  constructor(time: Date, coin: string, status: string) {
    super();
    this.time = time;
    this.coin = Reference.createFromPK(Coin, coin);
    this.status = status;
  }
}
