import { Entity, PrimaryKey, Property, types } from "@mikro-orm/core";
import { CrypcilBaseEntity } from "./BaseEntity";

@Entity()
export class Coin extends CrypcilBaseEntity {
  @PrimaryKey({ type: types.text })
  id!: string;

  @Property()
  symbol!: string;

  @Property()
  name!: string;

  @Property({ type: types.text })
  contract_address?: string;

  constructor(
    id: string,
    symbol: string,
    name: string,
    contract_address?: string
  ) {
    super();
    this.id = id;
    this.symbol = symbol;
    this.name = name;
    this.contract_address = contract_address;
  }
}
