import { seedHistoricalTokenPrice } from "./historical-daily-price-seeder";
import { seedTokens } from "./insert-tokens";

async function seed() {
  await seedTokens();
  await seedHistoricalTokenPrice();
}

seed();
