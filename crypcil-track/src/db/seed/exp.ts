import { MikroORM } from "@mikro-orm/core";
import { Coin } from "../entities/Coin";
import { CoinPrice } from "../entities/CoinPrice";
import ormConfig from "../mikro-orm.config";
import { TOKENS } from "./token";

export async function exp() {
  // initialize mikro-orm
  const orm = await MikroORM.init(ormConfig);

  // fork em
  const em = orm.em.fork();

  // seed token
  const z: CoinPrice[] = await em.execute(
    `SELECT DISTINCT ON (coin_id) * FROM coin_price ORDER BY coin_id, time DESC;`
  );
  console.log("Done query!", z);

  await orm.close();

  return true;
}

exp();
