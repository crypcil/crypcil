import { MikroORM } from "@mikro-orm/core";
import { CoinPrice } from "../entities/CoinPrice";
import ormConfig from "../mikro-orm.config";
import { TOKENS } from "./token";

import _ from "lodash";
import got from "got";

interface CoinGeckoHistoricalData {
  prices: [[number, number]];
  market_caps: [[number, number]];
  total_volumes: [[number, number]];
}

export async function seedHistoricalTokenPrice() {
  // initialize mikro-orm
  const orm = await MikroORM.init(ormConfig);

  // fork em
  const em = orm.em.fork();

  // seed historical price using for loops
  // not efficient but we need to avoid the rate limiting
  const seedPromises = Object.values(TOKENS).map(async ({ id }) => {
    const data: CoinGeckoHistoricalData = await got
      .get(
        `http://api.coingecko.com/api/v3/coins/${id}/market_chart?vs_currency=usd&days=max`
      )
      .json();

    const secondElement = (arr: [number, number][]) => arr.map((e) => e[1]);

    const toPersist = _.zip(
      data.prices.map((e) => e[0]),
      secondElement(data.prices),
      secondElement(data.market_caps),
      secondElement(data.total_volumes)
    ).map(([timestamp, price, market_cap, total_volume]) => {
      return new CoinPrice(
        new Date(timestamp!),
        id,
        price!,
        market_cap!,
        total_volume!
      );
    });

    em.persist(toPersist);

    console.log(`Done seeding ${id}`);
  });

  await Promise.all(seedPromises);

  await em.flush();

  console.log("Done seeding!");

  await orm.close();

  return true;
}
