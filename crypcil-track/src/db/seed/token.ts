export type TokenInfo = {
  id: string;
  name: string;
  token: string;
  address?: string;
  decimals: number;
};
export const MAINNET_TOKENS: Record<string, TokenInfo> = {
  BNB: {
    id: "binancecoin",
    name: "Binance Coin",
    token: "BNB",
    decimals: 18,
  },
  USDT: {
    id: "tether",
    name: "Tether USD",
    token: "USDT",
    address: "0x55d398326f99059fF775485246999027B3197955",
    decimals: 18,
  },
  ALPACA: {
    id: "alpaca-finance",
    name: "Alpaca Finance",
    token: "ALPACA",
    address: "0x8f0528ce5ef7b51152a59745befdd91d97091d2f",
    decimals: 18,
  },
  BELT: {
    id: "belt",
    name: "Belt Finance",
    token: "BELT",
    address: "0xe0e514c71282b6f4e823703a39374cf58dc3ea4f",
    decimals: 18,
  },
  BUNNY: {
    id: "pancake-bunny",
    name: "Pancake Bunny",
    token: "BUNNY",
    address: "0xc9849e6fdb743d08faee3e34dd2d1bc69ea11a51",
    decimals: 18,
  },
  CAKE: {
    id: "pancakeswap-token",
    name: "PancakeSwap",
    token: "CAKE",
    address: "0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82",
    decimals: 18,
  },
  BUSD: {
    id: "binance-usd",
    name: "Binance USD",
    token: "BUSD",
    address: "0xe9e7cea3dedca5984780bafc599bd69add087d56",
    decimals: 18,
  },
  TKO: {
    id: "tokocrypto",
    name: "Tokocrypto",
    token: "TKO",
    address: "0x9f589e3eabe42ebc94a44727b3f3531c0c877809",
    decimals: 18,
  },
  GALA: {
    id: "gala",
    name: "Gala",
    token: "GALA",
    address: "0x7ddee176f665cd201f93eede625770e2fd911990",
    decimals: 18,
  },
  BAKE: {
    id: "bakerytoken",
    name: "BakerySwap",
    token: "BAKE",
    address: "0xe02df9e3e622debdd69fb838bb799e3f168902c5",
    decimals: 18,
  },
  XVS: {
    id: "venus",
    name: "Venus",
    token: "XVS",
    address: "0xcf6bb5389c92bdda8a3747ddb454cb7a64626c63",
    decimals: 18,
  },
  BSW: {
    id: "biswap",
    name: "Biswap",
    token: "BSW",
    address: "0x965f527d9159dce6288a2219db51fc6eef120dd1",
    decimals: 18,
  },
  MBOX: {
    id: "mobox",
    name: "Mobox",
    token: "MBOX",
    address: "0x3203c9e46ca618c8c1ce5dc67e7e9d75f5da2377",
    decimals: 18,
  },
  BABY: {
    id: "babyswap",
    name: "BabySwap",
    token: "BABY",
    address: "0x53e562b9b7e5e94b81f10e96ee70ad06df3d2657",
    decimals: 18,
  },
  PACOCA: {
    id: "pacoca",
    name: "Pacoca",
    token: "PACOCA",
    address: "0x55671114d774ee99d653d6c12460c780a67f1d18",
    decimals: 18,
  },
};

export const TOKENS = MAINNET_TOKENS;

export const TOKEN_IDS = Object.values(TOKENS).map(({ id }) => id);

export const TOKEN_IDS_CSV = TOKEN_IDS.join(",");
