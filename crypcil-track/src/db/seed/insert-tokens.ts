import { MikroORM } from "@mikro-orm/core";
import { Coin } from "../entities/Coin";
import ormConfig from "../mikro-orm.config";
import { TOKENS } from "./token";

export async function seedTokens() {
  // initialize mikro-orm
  const orm = await MikroORM.init(ormConfig);

  // fork em
  const em = orm.em.fork();

  // seed token
  const tokensToPersist = Object.values(TOKENS).map(
    ({ id, name, token, address }) => {
      return new Coin(id, token, name, address);
    }
  );

  await em.persistAndFlush(tokensToPersist);
  console.log("Done seeding!");

  await orm.close();

  return true;
}
