import { Migration } from '@mikro-orm/migrations';

export class Migration20220527141633 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table "track_request" ("time" timestamptz not null, "coin_id" text not null, "status" varchar(255) not null);');
    this.addSql('alter table "track_request" add constraint "track_request_pkey" primary key ("time", "coin_id");');

    this.addSql('alter table "track_request" add constraint "track_request_coin_id_foreign" foreign key ("coin_id") references "coin" ("id") on update cascade;');
  }

  async down(): Promise<void> {
    this.addSql('drop table if exists "track_request" cascade;');
  }

}
