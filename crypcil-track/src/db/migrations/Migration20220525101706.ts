import { Migration } from '@mikro-orm/migrations';

export class Migration20220525101706 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table "coin" ("id" text not null, "symbol" varchar(255) not null, "name" varchar(255) not null, "contract_address" text null);');
    this.addSql('alter table "coin" add constraint "coin_pkey" primary key ("id");');

    this.addSql('create table "coin_price" ("time" timestamptz not null, "coin_id" text not null, "price" double precision not null, "market_cap" double precision null, "total_volume" double precision not null);');
    this.addSql('alter table "coin_price" add constraint "coin_price_pkey" primary key ("time", "coin_id");');

    this.addSql('alter table "coin_price" add constraint "coin_price_coin_id_foreign" foreign key ("coin_id") references "coin" ("id") on update cascade;');
  }

  async down(): Promise<void> {
    this.addSql('alter table "coin_price" drop constraint "coin_price_coin_id_foreign";');

    this.addSql('drop table if exists "coin" cascade;');

    this.addSql('drop table if exists "coin_price" cascade;');
  }

}
