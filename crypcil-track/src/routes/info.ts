import { FastifyPluginAsync, FastifyRequest } from "fastify";
import fp from "fastify-plugin";
import { getPrice } from "../db/dailyPriceCache";
import {harga, hargaPast, liveprice} from "../schemas/info";
import { getMatchingPrice } from "../services/priceget";
import { TOKEN_IDS, TOKENS } from "../db/seed/token";
import { TrackRequest } from "../db/entities/TrackRequest";
import { CoinPrice } from "../db/entities/CoinPrice";
import { MikroORM } from "@mikro-orm/core";
import ormConfig from "../db/mikro-orm.config";

// @ts-ignore
export const info: FastifyPluginAsync = fp(async (instance, options) => {
  // instance.log.info(options);
  instance.route<{ Params: { address: string } }>({
    method: "GET",
    url: "/track/harga",
    schema: harga,
    handler: async (request, reply) => {
      reply.send({
        status: "success",
        data: await getPrice(),
      });
    },
  });

  instance.route({
    method: "POST",
    url: "/track/harga_past",
    // schema: hargaPast,
    handler: async (request, reply) => {
      // console.log(request.orm)
      const em = request.orm.em;
      const queryData: { timestamp: string; coin_id: string }[] =
        request.body["data"];
      const prices = [];
      for (const query of queryData) {
        if (!TOKEN_IDS.includes(query.coin_id)) {
          reply.send({
            status: `${query.coin_id} doesn't exist`,
            available_coins: TOKEN_IDS,
          });
        }
        prices.push(
          await getMatchingPrice(
            query.coin_id,
            new Date(parseInt(query.timestamp)),
            em
          )
        );
      }
      reply.send({
        status: "success",
        prices,
      });
    },
  });

  instance.route({
    method: "POST",
    url: "/track/liveprice",
    // schema: liveprice,
    handler: async (request, reply) => {
      const channel = instance.amqp.channel;

      const em = request.orm.em;
      const queryData: { token: string } = request.body["data"];

      const token = queryData.token;

      if (!Object.keys(TOKENS).includes(token)) {
        reply.send({
          status: `${token} is not supported`,
        });
      }

      const q = await channel.assertQueue("", {
        exclusive: true,
      });

      const correlationId =
        Math.random().toString() +
        Math.random().toString() +
        Math.random().toString();

      instance.log.info(`Requesting trackToken(${token})`);

      const trackRequest = new TrackRequest(
        new Date(),
        TOKENS[token].id,
        "tracking"
      );

      em.persistAndFlush(trackRequest);

      channel.consume(
        q.queue,
        async function (msg) {
          const orm = await MikroORM.init(ormConfig);
          const em = await orm.em.fork();

          if (msg.properties.correlationId == correlationId) {
            const livePriceData = msg.content.toString();
            const livePrice = JSON.parse(livePriceData);
            const coinPrices = livePrice.forEach(
              ([timestamp, price]) =>
                new CoinPrice(
                  new Date(timestamp),
                  TOKENS[token].id,
                  price,
                  -1,
                  -1
                )
            );
            await em.persist(coinPrices);
            trackRequest.status = "done";
            await em.persistAndFlush(trackRequest);
            await em.flush();
            instance.log.info(
              `Received liveprice data for ${token} with ${livePrice.length} datapoints!`
            );
          }

          await orm.close();
        },
        {
          noAck: true,
        }
      );

      instance.log.info(
        `Sending liveprice track request for ${token} to queue!`
      );
      channel.sendToQueue("liveprice_queue", Buffer.from(token.toString()), {
        correlationId: correlationId,
        replyTo: q.queue,
      });

      reply.send({
        status: "success",
      });
    },
  });
});
