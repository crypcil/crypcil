/* eslint-disable no-unused-vars */
import { MikroORM, Options } from "@mikro-orm/core";
import { PostgreSqlDriver } from "@mikro-orm/postgresql";
import { Channel, Connection } from "amqplib";

export declare namespace fastifyMikroOrm {
  type Awaited<T> = T extends PromiseLike<infer U> ? Awaited<U> : T;

  type FastifyMikroOrmOptions = {
    orm?: MikroORM<PostgreSqlDriver>;
  };

  type MikroORMPluginOptions = Options<PostgreSqlDriver> &
    FastifyMikroOrmOptions;
}

declare module "fastify" {
  interface FastifyInstance {
    orm: fastifyMikroOrm.Awaited<MikroORM<PostgreSqlDriver>>;
    amqp: {
      connection: Connection;
      channel: Channel;
    };
  }
  interface FastifyRequest {
    orm: fastifyMikroOrm.Awaited<MikroORM<PostgreSqlDriver>>;
  }
}
