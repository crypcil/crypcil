import fp from "fastify-plugin";
import type { FastifyPluginAsync } from "fastify";
import type { fastifyMikroOrm } from "./types";
import { MikroORM } from "@mikro-orm/core";
import { PostgreSqlDriver } from "@mikro-orm/postgresql";

const fastifyMikroORM: FastifyPluginAsync<fastifyMikroOrm.MikroORMPluginOptions> =
  async function (fastify, options) {
    try {
      let orm = options.orm
        ? options.orm
        : await MikroORM.init<PostgreSqlDriver>(options);

      fastify.decorate("orm", orm);

      fastify.addHook(
        "onRequest",
        async function (this: typeof fastify, request, reply) {
          request.orm = Object.assign({}, this.orm); // copy orm
          request.orm.em = request.orm.em.fork();
        }
      );

      fastify.addHook("onClose", () => orm.close());

      fastify.log.info("MikroORM plugin loaded");
    } catch (e) {
      fastify.log.error("Error initializing MikroORM");
      fastify.log.error(e);
    }
  };

const mikroORMPlugin = fp(fastifyMikroORM, { name: "fastify-mikro-orm" });

export default mikroORMPlugin;
