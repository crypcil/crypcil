import fp from "fastify-plugin";
import amqpClient from "amqplib";

function getTarget({
  frameMax,
  heartbeat,
  hostname,
  locale,
  password,
  port,
  url,
  username,
  vhost,
}) {
  if (url) {
    return url;
  } else if (hostname) {
    return {
      frameMax,
      heartbeat,
      hostname,
      locale,
      password,
      port,
      username,
      vhost,
    };
  } else {
    throw new Error("`url` parameter is mandatory if no hostname is provided");
  }
}

async function fastifyAmqp(fastify, options) {
  const connection = await amqpClient.connect(
    getTarget(options),
    options.socket
  );
  fastify.addHook("onClose", () => connection.close());

  const channel = await connection.createChannel();

  fastify.decorate("amqp", {
    connection,
    channel,
  });
}

export default fp(fastifyAmqp, {
  fastify: ">=1.0.0",
  name: "fastify-amqp",
});
